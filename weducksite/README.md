We Duck
=========

Michael Wenting & Joost Röttger

Opstarten
------
1. Plaats de map __weducksite__ in __workspaces__ op de server
2. Open _server/config/config.js_, wijzig het poortnummer en de databasenaam
3. Log in op de server met Putty 
4. Ga naar de map _workspaces/weducksite/server_
5. Installeer npm en de npm supervisor met ```npm install``` en ```npm install -g supervisor```
6. Ga naar _http://autobay.tezzt.nl/rockmongo/_ en log in.
7. Kies je eigen database die je ook hebt gebruikt in _config.js_.
8. Klik op __import__ rechtsboven in en open _rockmongo-export-P507388-1389704854.js_
9. Start de server door ```supervisor --no-restart-on error server.js``` in te voeren
10. De server wordt gestart!

API
------
Bijgevoegd vind je het bestand _APIrequest.docx_, hier in staat een beschrijving van de API. De request vind je terug via deze link:
_https://www.getpostman.com/collections/732f31f55a5a38645ef0_.
Sla de tekst op en importeer het in postman.

Import data
------
Volg voor het importen van de data de volgende stappen:

1. Verplaats p123456 in je eigen database
2. Importeer de data per collectie of in een stap
3. Klaar

```
mongoimport --db  p123456  --collection users --file users.bson
mongoimport --db  p123456  --collection stories --file users.metadata.json
mongoimport --db  p123456  --collection scenarios --file configurator.bson
mongoimport --db  p123456  --collection scenes --file configurator.metadata.json
```
Of importeer alle data ineens:
```
mongorestore -d p123456 ~/workspaces/weducksite/data
```

Jasmine-node testing
------
Server-side testing doormiddel van Jasmine-node. Als Jasmine-node nog niet is geinstalleerd, volg dan de onderstaande stappen:
```
1. npm install jasmine-node -g
2. npm install request -g
```
Nadat deze stappen zijn voltooid, kunnen de testen worden uitgevoerd doormiddel van het volgende commando:
```
jasmine-node spec/
```
Beschrijving van functies
------
```javascript
function contactCtrl ($scope, $http) {
    $scope.contactForm = function (contactForm) 
        [..]
        $http({
            method: "PUT",
            url: "/contact/" + contactForm.username + "/sendQuestion",
            data: {"name":contactForm.nameCustomer, "mail": contactForm.username, "subject": contactForm.subject, "question": contactForm.question  }
        }).success(function () {
                console.log("email verstuurd: ");

            });
    }

}```
In deze functie wordt een put-methode uitgevoerd, waarmee er een e-mail wordt gestuurd naar We Duck wanneer de gebruiker het formulier op de site invuld. Zie voor het request de API-documentatie.



```javascript
function getUserDetails($location, $http, $scope, configService, usersService, $routeParams) {
    var isSuccess;
    $scope.users = usersService.users.get({email: $routeParams.email});
    [..]
    if ($routeParams.email !== '0') {
        document.getElementById('passwordDiv').style.display = 'none';
        document.getElementById('inlogForm').style.display = 'none';

    }
    if ($routeParams.email === '0') {
        document.getElementById('changePasswordDiv').style.display = 'none';
        document.getElementById('getDuckList').style.display = 'none';
    }
```
Deze functie haalt de gegevens van de gebruiker op aan de hand van het mailadres waarmee is ingelogd. Als er niet is ingelogd, worden er geen gegevens getoond en moet er eerst ingelogd worden.
```javascript
    $scope.delete = function () {
        usersService.users.delete({email: $routeParams.email});
        $location.path("/users");
    }```
In deze functie wordt een gebruiker opgehaald aan de hand van het mailadres en verwijderd met een delete-methode
```javascript
$scope.save = function (userForm) {
        if ($scope.users.doc && $scope.users.doc._id !== undefined) {
            usersService.users.update({email: $scope.users.doc.email}, $scope.users.doc, function (res) {
                isSuccess = res.err === null;
                $scope.isSuccess = isSuccess;
                if (isSuccess) {
                    $scope.msg = "User successfully updated.";
                } else {
                    $scope.err = res.err.message;
                    $scope.err = res.err.message;
                }
            });
        } else {
            console.log('Entering save', $scope.users.doc);
            usersService.users.save({}, $scope.users.doc, function (res) {
                isSuccess = res.err === null;
                if (isSuccess) {
                    [..]
                } else {
                    $scope.err = res.err.message;
                    $scope.err = res.err.message;
                    [..]
                }
            });
        }
    }
```
Met deze functie wordt een put-methode uitgevoerd waarmee de gewijzigde gegevens van de gebruiker worden opgeslagen, mits er aan de voorwaarden wordt voldaan. Oftewel: Er moet een id zijn en de gebruiker moet bestaan in de database.

```Javascript
$scope.changePassword = function (changeForm) {
    [..]
        $http({
            method: "POST",
            url: "/myLogin",
            data: {"username": $scope.users.doc.email, "password": oldPassword}
        }).success(function (data) {
                var isVerified = data.isVerified;
                if (isVerified === true) {
                    if (newPassword !== undefined && confirmPassword !== undefined) {
                        console.log("wachtwoorden ingevuld check");
                    } else {
                        console.log("ww zijn niet ingevuld");
                    }
                    if (newPassword === confirmPassword && changeForm.newPassword.length > 1 && changeForm.confirmPassword.length > 1) {
                        [..]
                        $http({
                            method: "PUT",
                            url: "/users/" + $scope.users.doc.email + "/password",
                            data: {"password": confirmPassword}
                        }).success(function (data) {
                                if (data.isVerified === true) {
                                    console.log("new " + $scope.users.doc.password);

                                }
                            });

                    } else {
                        console.log("ww komen niet overeen of voldoen niet");
                    }
                } else {
                    console.log("oude wachtwoord komt niet overeen");
                }
                
                if (isVerified === true) {
                    console.log("bingo");
                    if (newPassword !== null && confirmPassword !== null && newPassword === confirmPassword) {
                        console.log("new password= " + confirmPassword);
                    }
                    else {
                        console.log("nieuwe wachtwoorden komen niet overeen");
                    }
                } else {
                    console.log("Het huidige wachtwoord is niet correct");
                }
            });
    }
```
In deze functie wordt het wachtwoord met een put-methode opgeslagen, als er aan voorwaarden wordt voldaan. Er zijn verschillende fouten mogelijk, namelijk: wachtwoord en wachtwoord herhalen komen niet overeen, de gebruiker is niet ingelogd of een veld is leeg.
```Javascript
    $scope.getDetails = function (id) {
        var id = id;
        $scope.configurator = configService.configurator.get({_id: id});
        [..]
    }
```
De functie haalt een eendje op, waarna het in de weggelaten code wordt opgemaakt.
```Javascript
    $scope.getBack = function () {
        [..]
    }

    $scope.getEdit = function (duckId) {
        [..]
        $location.path("/configurator/" + duckId);
    }
    
    $scope.makeOrder = function (id) {
        [..]
        window.location = "#/transaction/id/" + id;
    }
```
Deze drie functies zijn nodig voor het verbergen en weergeven van elementen.
```Javascript
function winkelmandCtrl($http, usersService, transactionService, configService, $scope) {
    $http({
        method: "GET",
        url: "/account"
    }).success(function (data) {
            var i, isVerified = data.isVerified;
            $scope.users = usersService.users.get({email: data.user.email });
            [..]
            $http({
                method: "GET",
                url: "/users/" + data.user.email
            }).success(function (data) {
                    for (i = 0; i < data.doc.transaction.length; i = i + 1) {
                        $http({
                            method: "GET",
                            url: "/transactions/" + data.doc.transaction[i]._id
                        }).success(function (data) {
                                if (data.doc.delivered !== "Ja") {
                                    var array = [data.doc._id];
                                    for (i = 0; i < array.length; i = i + 1) {

                                        $scope.transactions = transactionService.transactions.get({_id: array[i] });

                                    }


                                }
                            });
                    }
                });

        });

    $scope.getDetails = function (transactionID) {
        console.log("details: " + transactionID);
        window.location = "#/winkelmand/" + transactionID;


    }

}
```
Deze functie haalt de gebruiker op aan de hand van het e-mailadres waarmee is ingelogd en haalt daarbij de transacties op die gekoppeld zijn aan deze gebruiker.
```Javascript
function wallOfDuckCtrl($scope, wallService, $location) {
    var wallId = "52c985cffb13748441000019";
    console.log("wall of duck - under construction");
    $scope.wallofduck = wallService.wallofduck.get({_id: wallId});

    $scope.getDesign = function (duckId) {
        console.log("Get design of: " + duckId);

        $location.path("/configurator/" + duckId);

    }

    var items, images, i, page = 0, numPag = 0;


    images = setOptions.image();
    createItems();
    scrollButtons();


    function createItems() {
        var div, gallery, img, firstItem, lastItem;
        gallery = document.getElementById("gallery");
        console.log("create items");
        numPag = Math.ceil(images.length / 6) - 1;
        if (images.length > 6) {
            console.log(numPag);
            firstItem = page * 6;
            lastItem = firstItem + 6;

        }
        for (i = firstItem; i <= lastItem; i = i + 1) {
            div = document.createElement("div");
            div.setAttribute('class', 'galleryItem');
            div.setAttribute('id', 'galleryItem' + i);
            img = document.createElement("img");
            img.setAttribute('src', 'images/' + images[i]);
            div.appendChild(img);
            gallery.appendChild(div);
        }
        scrollNav();
    }

    function removeItems() {
        var items;
        console.log("remove items");
        items = document.getElementsByClassName("galleryItem");
        for (i = 0; i < items.length; i) {
            items[i].parentNode.removeChild(items[i]);
        }
    }

    function scrollButtons() {
        var buttons;
        buttons = document.getElementsByClassName("scrollButton");
        for (i = 0; i < buttons.length; i = i + 1) {
            buttons[i].onmousedown = scrollButtonClick(buttons[i]);
        }


    }

    function scrollButtonClick(button) {
        return function (event) {
            numPag = parseInt(numPag, 10);
            if (button.id === "scrollLeft") {
                page = page - 1;
            } else {
                page = page + 1;
            }
            console.log(numPag);
            if (page < 0) {
                page = numPag;
            }
            if (page > numPag) {
                page = 0;
            }
            console.log(page);
            removeNav();
            removeItems();
            createItems();
        };
    }

    function scrollNav() {

        var img, dot, scrollNav = document.getElementById("scrollNav");
        for (i = 0; i <= numPag; i = i + 1) {
            if (i === page) {
                dot = "dot2.png";
            } else {
                dot = "dot.png";
            }
            img = document.createElement("img");
            img.setAttribute('class', 'scrollNavButton');
            img.setAttribute('src', 'images/' + dot);
            scrollNav.appendChild(img);
        }

    }

    function removeNav() {
        console.log("removenav");
        var scrollNav = document.getElementsByClassName("scrollNavButton");
        console.log(scrollNav);
        for (i = 0; i < scrollNav.length; i) {
            scrollNav[i].parentNode.removeChild(scrollNav[i]);
        }
    }
}
```
De badeendjes die aan de Wall of Duck zijn toegevoegd worden opgehaald. De functies in deze controller zijn voor het aanmaken van elementen en het koppelen van eventhandlers aan elementen. Deze functies wijzen voor zich.
```Javascript
function transactionDetailCtrl($scope, $http, $location, usersService, configService, transactionService, $routeParams) {
    var isSuccess, price, input, select, units, present;

    $scope.configurator = configService.configurator.get({id: $routeParams._id});

    $scope.present = [
        "Ja", "Nee"
    ];
    $scope.banken = [
        "ABN AMRO", "Rabobank", "ING", "ASN Bank"
    ];

    $scope.present = [
        "Nee", "Ja (+ € 1,99)"
    ];

    //GET USER DETAILS
    $http({
        method: "GET",
        url: "/account"
    }).success(function (data) {
            $scope.users = usersService.users.get({email: data.user.email});
        });
    $scope.delete = function () {
        transactionService.transactions.delete({id: $routeParams._id});
        $location.path("/configurator");
    }
    console.log("route" + $routeParams._id)

    $scope.configurator = configService.configurator.get({_id: $routeParams._id});
```
In deze functie worden variablen gevuld met arrays, die de opties zijn voor het bestelling afronding. De gebruiker wordt wederom opgehaald aan de hand van het e-mailadres waarmee de gebruiker is ingelogd. Deze wordt gebruikt om de zijn/haar gegevens te tonen voor de bestelling.
```Javascript
$scope.updateUser = function () {
        console.log("update");
        var email = $scope.users.doc.email;
        var name = $scope.users.doc.name;
        var street = $scope.users.doc.street;
        var zip = $scope.users.doc.zip;
        var state = $scope.users.doc.state;
        var bankName = $scope.users.doc.bankName;
        var reknumber = $scope.users.doc.number;


        $http({
            method: "PUT",
            url: "/users/" + $scope.users.doc.email,
            data: {"email": email, "name": name, "street": street, "zip": zip, "state": state, "bankName": bankName, "number": reknumber}
        }).success(function () {
                console.log("update user: " + $scope.users.doc.name);
            });
    }
```
De gewijzigde gegevens worden opgeslagen met een put-methode
```Javascript
$scope.price = function () {
        console.log("size eend: " + $scope.configurator.doc.size);

        input = document.getElementById("input");
        select = document.getElementById("select")
        price = $scope.configurator.doc.size + 2.99;
        units = input.value;
        present = select.value;
        console.log(present);
        input = input.value * price;
        select = select.value * 1.99;
        console.log(price, input, select);
        price = input + select;
        console.log(price);
        document.getElementById('totalPrice').innerHTML = '<br>' + 'Totale Prijs: € ' + price.toFixed(2);
    }

```
De prijs voor de badeendjes in het bestelforulier worden gecalculeerd met deze functie.
```Javascript
$scope.bestel = function () {
        var delivered = "Nee";
        console.log("aantal eendjes: " + units + ", wel of geen cadeau: " + present);

        console.log("relatie: " + $scope.users.doc._id);
        console.log("productid: " + $routeParams._id);

        $http({
            method: "POST",
            url: "/transactions",
            data: {"units": units, "present": present, "price": price, "delivered": delivered }
        }).success(function (res) {
                console.log("new order!");
                var id = $scope.get = res.doc._id;

                $http({
                    method: "PUT",
                    url: "/transactions/" + id + "/addIDS",
                    data: {"transactionId": id, "relationID": $scope.users.doc._id, "productID": $routeParams._id}
                }).success(function () {
                        // window.location = "#/winkelmand"

                        $http({
                            method: "PUT",
                            url: "/users/" + $scope.users.doc.email + "/addTransaction",
                            data: {"transactionID": id}
                        }).success(function () {


                            });
                    });
            });

    }
}
```
Bij het afronden van de bestelling wordt de transactie weggeschreven naar de database.
```Javascript
function makeConfiguratorDetail($http, usersService, configService, $routeParams, $scope) {
    var duckSize, bCanPreview, canvas, hexColor, duckAccessoires, duckColor, duckVolume, isSuccess, duckPattern, color, cld, i, block, viewer, colors = [], patterns = [], sizes = [], accessoires = [], setItem, setColor = "#FFFFFF";
    console.log("get duck and updateeeee");

    $scope.configurator = configService.configurator.get({_id: $routeParams._id});

    $http({
        method: "GET",
        url: "/account"
    }).success(function (data) {
            $scope.users = usersService.users.get({email: data.user.email});
        });

    // LOAD DUCK
    $http({
        method: "GET",
        url: "/configurator/" + $routeParams._id
    }).success(function (data) {
        [..]
        });

    $scope.update = function () {
        [..]
        $http({
            method: "PUT",
            url: "/configurator/" + $routeParams._id,
            data: {"size": duckSize, "volume": duckVolume, "pattern": duckPattern, "accessoires": duckAccessoires, "color": duckColor}
        }).success(function () {
                console.log("duck upgedate");

            });
    }
    
    $scope.product = function () {
        $http({
            method: "GET",
            url: "/configurator/" + $routeParams._id,
        }).success(function (data) {
                [..]
                window.location = "#/transaction/id/" + data.doc._id;
            });

    }
}
```
In deze controller wordt de configurator gebouwd. Op de plek van de eerste [..] staan tientallen functies waarin elementen aangemaakt worden, verwijderd worden en eventhandlers krijgen. Zie de code voor commentaar per functie. Verder bevat het de functie "update" die het ontworpen badeendje weg schrijft naar de database.
```Javascript
function UserLoginCtrl($scope, $http, usersService) {
    var isVerified, partial, thisUser, password;
    $scope.loginCtrl = function () {
        document.getElementById("loginCtrl").setAttribute("style", "display:block");
    }
    usersService.users.get({action: "checkLogin"}, function (req) {
        isVerified = req.isVerified;
    });

    $scope.getPartial = function () {
        //console.log('getPartial ', isVerified);
        if (isVerified === undefined) {
            $http({
                method: "GET",
                url: "/account"
            }).success(function (data) {
                    $scope.users = usersService.users.get({email: data.user.email});
                    isVerified = data.isVerified;

                    if (isVerified === true) {
                        partial = "/partials/logout.html";
                    } else {
                        partial = "/partials/login.html";
                    }
                    $scope.src = partial;
                    return partial;
                });
        }

        if (isVerified === true) {
            partial = "/partials/logout.html";
        }
        else {
            partial = "/partials/login.html";
        }
        $scope.src = partial;
        return partial;
    };
```
In deze controller wordt de partial opgehaald, afhankelijk van of je ingelogd bent of niet.
```Javascript
$scope.login = function (loginForm) {
        console.log('login', loginForm)

        $http({
            method: "POST",
            url: "/myLogin",
            data: {"username": loginForm.username, "password": loginForm.password}
        }).success(function (data) {
                isVerified = data.isVerified;
                thisUser = loginForm.username;
                password = loginForm.password;

                if (isVerified === true) {
                    // load partial
                    partial = "/partials/logout.html";
                    $scope.src = partial;
                    // redirect to user area
                    window.location = "#/home/";
                }
                if (thisUser === "admin@weduck.nl" && password === "adminWeDuck") {
                    partial = "/partials/logoutAdmin.html";

                    window.location = "#/adminPanel/";
                    console.log("Welcome admin");
                } else {
                    console.log('No login...');
                    console.log(data);
                    window.location = "#/";
                }

            });
    };
```
De functie laadt de juiste partipal als je ingelogd bent.
```Javascript
$scope.goToAccount = function () {
        $http({
            method: "GET",
            url: "/account"
        }).success(function (data) {
                window.location = "#/users/" + data.user.email;
                console.log("user: " + data.user.email);
            });
    }
```
In deze functie wordt de juiste gebruiker opgehaald uit de database.
```Javascript
    $scope.logout = function () {

        $http({
            method: "GET",
            url: "/logout"
        }).success(function (data) {
                isVerified = data.isVerified;

                if (isVerified === true) {
                    $scope.src = partial;

                    window.location = "#/home/";
                } else {
                    console.log('log out');
                    partial = "/partials/login.html";
                    window.location = "#/home/";
                }
            });
    };
    
```
Deze functie logt de gebruiker uit en stuurt de gebruiker naar de homepage.
```Javascript
    $scope.openLogin = function () {
        [..]
    };

    $scope.cancelLogin = function () {
        [..]
    };
    
    $scope.openForm = function () {
        [..]
    }
```
uitleg
```Javascript
    $scope.forgotPassword = function (forgotForm) {
        $http({
            method: "GET",
            url: "/users"
        }).success(function (data) {
                var stopLoop = false;
                for (var i = 0; i < data.doc.length; i = i + 1) {
                    if (stopLoop == false) {
                        if (forgotForm.username !== data.doc[i].email) {
                        } else {
                            stopLoop = true;
                            console.log("id vd klant: " + data.doc[i]._id + ",emailadress vd klant: " + data.doc[i].email);
                            var newGenPassword = Math.random().toString(36).slice(-8);
                            $http({
                                method: "PUT",
                                url: "/users/" + data.doc[i].email + "/password",
                                data: {"password": newGenPassword}
                            }).success(function () {
                                    if (data.isVerified === true) {
                                        console.log("nieuw wachtwoord: " + newGenPassword);
                                    }
                                });
                            $http({
                                method: "PUT",
                                url: "/users/" + data.doc[i].email + "/sendPassword",
                                data: {"username": data.doc[i].email, "newPassword": newGenPassword}
                            }).success(function () {
                                    if (data.isVerified === true) {
                                        console.log("nieuw wachtwoord: " + newGenPassword);
                                    }
                                });
                        }
                    }
                }
            });

    }
```
Deze functie verwerkt de "wachtwoord vergeten"-aanvraag.
```Javascript
    $scope.setNav = function (id) {
        var icons, icon, newImg;
        icons = document.getElementsByClassName("iconNavigation");
        console.log("icons: ", icons);
        $http({
            method: "GET",
            url: "/account"
        }).success(function (data) {
                var isVerified = data.isVerified;

                if (isVerified === true) {
                    console.log("ingelogd", icons);
                    for (var i = 0; i < icons.length; i++) {
                        console.log("icons: ", icons[i]);
                        newImg = icons[i].src.slice(0, -5)
                        newImg = newImg + "1.png";
                        icons[i].src = newImg;

                    }
                } else {
                    for (var i = 1; i < icons.length; i++) {
                        console.log("icons: ", icons[i]);
                        newImg = icons[i].src.slice(0, -5)
                        newImg = newImg + "1.png";
                        icons[i].src = newImg;
                        console.log("uitgelogd");
                    }
                }
                icon = document.getElementById(id);
                newImg = icon.childNodes[0].src.slice(0, -5)
                newImg = newImg + "2.png";
                icon.childNodes[0].src = newImg;


            });

    }
}
```
In deze functie wordt de opmaak van de hoofdnavigatie geregeld. Wanneer er op een icoon geklikt wordt, wordt deze functie aangeroepen en veranderd het icoontje in een andere kleur.

