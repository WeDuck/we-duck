/**
 * Created by Joost Rottger on 03-11-13.
 */

var mongoose = require('mongoose'),
    wall = mongoose.model('Wall');

// CREATE
exports.create = function (req, res) {
    var doc = new wall(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
}

// RETRIEVE

// Retrieve all
exports.list = function (req, res) {
    var condition, fields, options;

    condition = {};
    fields = {};
    sort = {'modificationDate': -1};

    wall
        .find(condition, fields)
        .sort(sort)
        .exec(function (err, doc){
            var retObj = {
                meta: {"action": "list",'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}
// Retrieve one
exports.detail = function (req, res) {
    var condition, fields, options;

    condition = {_id: req.params._id},
        fields = {},
        options = {'createdAt': -1};

    wall
        .find(condition, fields)
        .exec(function (err, doc){
            var retObj = {
                meta: {"action": "list",'timestamp': new Date()},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        })
}

// UPDATE
exports.update = function (req, res) {
    var item = {
        _id: req.body.duckiesId
    };
    var conditions =
        {_id: req.params._id}
        , update = {
            $push: {duckies: item},
            safe: true
        }
        , options = { multi: true }
        , callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };
    wall.findOneAndUpdate(conditions, update, options, callback);
}

// DELETE
exports.delete = function (req, res) {
    var conditions, callback, retObj;

    conditions = {_id: req.params._id}
        , callback = function (err, doc) {
        retObj = {
            meta: {"action": "delete", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    }
    wall.remove(conditions, callback);
}
