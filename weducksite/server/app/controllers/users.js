/**
 * Created by theotheu on 27-10-13.
 */

var mongoose = require('mongoose')
    , User = mongoose.model('User')
    , passwordHash = require('password-hash');


// CREATE
// save @ http://mongoosejs.com/docs/api.html#model_Model-save
exports.create = function (req, res) {

    // Encrypt password
    req.body.password = passwordHash.generate(req.body.password || "admin");
    console.log('CREATE user.');

    var doc = new User(req.body);


    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
}


// RETRIEVE
// find @ http://mongoosejs.com/docs/api.html#model_Model.find
exports.list = function (req, res) {
    var conditions, fields, options;

    console.log('GET users.');

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    User
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}

exports.detail = function (req, res) {
    var conditions, fields, options;

    console.log('GET 1 user. ' + req.params.email);

    conditions = {email: req.params.email}
        , fields = {}
        , options = {'createdAt': -1};

    User
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date()},
                doc: doc[0], // only the first document, not an array
                err: err
            };
            return res.send(retObj);
        })
}
exports.login = function (req, res) {
    var conditions, fields, options;

    conditions = {
        name: req.body.name,
        p: req.params.name
    }
        , fields = {}
        , options = {'createdAt': -1};


    User
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {

                meta: {"action": "login", 'timestamp': new Date(), conditions: conditions},
                doc: {"isVerified": doc.length != 0},
                err: err
            };
            return res.send(retObj);
        })
}

// UPDATE
// findOneAndUpdate @ http://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
exports.update = function (req, res) {

    console.log('UPDATE user.');

    // Make sure that password is hashed.
    req.body.password = passwordHash.generate(req.body.password || "admin");

    var conditions =
        {email: req.params.email}
        , update = {
            name: req.body.name,
            street: req.body.street,
            zip: req.body.zip,
            state: req.body.state,
            email: req.body.email,
            bankName: req.body.bankName,
            number: req.body.number
        }
        , options = { multi: true }
        , callback = function (err, doc) {
            User.findByIdAndUpdate(req.body._id, update, function (err, me) {
                var retObj = {
                    meta: {"action": "update", 'timestamp': new Date()},
                    doc: me,
                    err: err
                };
                return res.send(retObj);
            });
        };
    User.findOneAndUpdate(conditions, update, options, callback);
}

// UPDATE PASSWORD
exports.updatePassword = function (req, res) {

    console.log('UPDATE PASSWORD user.');

    // Make sure that password is hashed.
    req.body.password = passwordHash.generate(req.body.password || "admin");

    var conditions =
        {email: req.params.email}
        , update = {
            password: req.body.password
        }
        , options = { multi: true }
        , callback = function (err, doc) {
            User.findByIdAndUpdate(req.body._id, update, function (err, me) {
                var retObj = {
                    meta: {"action": "update", 'timestamp': new Date()},
                    doc: me,
                    err: err
                };
                return res.send(retObj);
            });


        };

    User.findOneAndUpdate(conditions, update, options, callback);
}


// DELETE
// remove @ http://mongoosejs.com/docs/api.html#model_Model-remove
exports.delete = function (req, res) {
    var conditions, callback, retObj;

    console.log('Deleting user. ', req.params.email);

    conditions = {email: req.params.email}
        , callback = function (err, doc) {
        retObj = {
            meta: {"action": "delete", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    }

    User.remove(conditions, callback);
}

exports.sendPassword = function (req) {
    var nodemailer = require("nodemailer");

    var smtpTransport = nodemailer.createTransport("SMTP");
    var mailTo = req.body.username;
    var newPassword = req.body.newPassword;

    var htmlStr = "Geachte Klant," + "<br/>" + "<br/>" + "U nieuwe wachtwoord is:  " + "<b>" + newPassword + "</b>" + "<br/>" + "<br/>" +
        "Met vriendelijke groet," + "<br/>" +
        "weduck.nl" + "<br/>"



    var mailOptions = {
        from: "weduck.nl <weDuck@info.nl>",
        to: mailTo,
        subject: "Uw wachtwoord",
        text: htmlStr,
        html: htmlStr
    };

    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + response.message);
        }
        smtpTransport.close();
    })
}

exports.updateUserWithDucks = function (req, res) {

    console.log('ADD DUCK.');

    var item = {
        _id: req.body.configId,
    };

    var conditions =
        {email: req.params.email}
        , update = {
            $push: {configurators: item},
            safe: true
        }
        , options = { multi: false }
        , callback = function (err, doc) {
            User.findByIdAndUpdate(req.body._id, update, function (err, me) {
                var retObj = {
                    meta: {"action": "update", 'timestamp': new Date()},
                    doc: me,
                    err: err
                };
                return res.send(retObj);
            });
        };

    User.findOneAndUpdate(conditions, update, options, callback);
}

exports.updateUserWithTransaction = function (req, res) {

    console.log('ADD TRANSACTION.');

    var item = {
        _id: req.body.transactionID
    };

    var conditions =
        {email: req.params.email}
        , update = {
            $push: {transaction: item},
            safe: true
        }
        , options = { multi: false }
        , callback = function (err, doc) {
            User.findByIdAndUpdate(req.body._id, update, function (err, me) {
                var retObj = {
                    meta: {"action": "update", 'timestamp': new Date()},
                    doc: me,
                    err: err
                };
                return res.send(retObj);
            });
        };

    User.findOneAndUpdate(conditions, update, options, callback);
}






