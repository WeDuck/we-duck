/**
 * Created by Joost Rottger on 03-11-13.
 */
"use strict"

var mongoose = require('mongoose'),
    configurator = mongoose.model('Configurator');

// CREATE

exports.create = function (req, res) {
    var doc, duckDetail = {
        color: req.body.color || '',
        size: req.body.size,
        pattern: req.body.pattern || '',
        volume: req.body.volume || '',
        text: req.body.text || '',
        accessoires: req.body.accessoires || '',
        image: req.body.image || '',
        configImage: req.body.configImage
    };

    doc = configurator(duckDetail);
   console.log(doc);
    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
}


// RETRIEVE

// Retrieve all
exports.list = function (req, res) {
    var condition, fields, options, sort;

    condition = {};
    fields = {};
    sort = {'modificationDate': -1};

    configurator
        .find(condition, fields)
        .sort(sort)
        .exec(function (err, doc){
            var retObj = {
                meta: {"action": "list",'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}
// Retrieve one
exports.detail = function (req, res) {
    var condition, fields, options;

    condition = {_id: req.params._id},
        fields = {},
        options = {'createdAt': -1};

    configurator
        .find(condition, fields)
        .exec(function (err, doc){
            var retObj = {
                meta: {"action": "list",'timestamp': new Date()},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        })
}

// UPDATE
exports.update = function (req, res) {

    var conditions =
        {_id: req.params._id}
        , update = {
            color: req.body.color,
            size: req.body.size,
            pattern: req.body.pattern || '',
            volume: req.body.volume || '',
            text: req.body.text || '',
            accessoires: req.body.accessoires,
            image: req.body.image || ''

        }

        , options = { multi: true }
        , callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };
    configurator.findOneAndUpdate(conditions, update, options, callback);
}

// DELETE
exports.delete = function (req, res) {
    var conditions, callback, retObj;

    conditions = {_id: req.params._id}
        , callback = function (err, doc) {
        retObj = {
            meta: {"action": "delete", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    }
    configurator.remove(conditions, callback);
}
