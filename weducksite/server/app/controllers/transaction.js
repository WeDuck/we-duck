/**
 * Created by Joost Rottger on 03-11-13.
 */

var mongoose = require('mongoose'),


    transactions = mongoose.model('Transaction');

// CREATE
exports.create = function (req, res) {
    var doc, transactionDetail = {
        unitsOrdered: req.body.units,
        present: req.body.present,
        price: req.body.price,
        delivered: req.body.delivered
    };
    doc = new transactions(transactionDetail);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
}

// RETRIEVE

// Retrieve all
exports.list = function (req, res) {
    var condition, fields, options;

    condition = {};
    fields = {};
    sort = {'modificationDate': -1};

    transactions
        .find(condition, fields)
        .sort(sort)
        .exec(function (err, doc){
            var retObj = {
                meta: {"action": "list",'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}
// Retrieve one
exports.detail = function (req, res) {
    var condition, fields, options;


    condition = {_id: req.params._id},
        fields = {},
        options = {'createdAt': -1};

    transactions
        .find(condition, fields)
        .exec(function (err, doc){
            var retObj = {
                meta: {"action": "list",'timestamp': new Date()},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        })
}

// UPDATE
exports.update = function (req, res) {

    var conditions =
        {_id: req.params._id}
        , update = {
            present: req.body.present || 'nee',
            unitsOrdered: req.body.unitsOrdered || '1'
        }
        , options = { multi: true }
        , callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };
    transactions.findOneAndUpdate(conditions, update, options, callback);
}
exports.addIDS = function (req, res) {

    var productId = {
        _id: req.body.productID
    };
    var relationId = {
        _id: req.body.relationID
    };

    var conditions =
        {_id: req.body.transactionId}
        , update = {
            $push: {products: productId, relations: relationId},
            safe: true
        }
        , options = { multi: true }
        , callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };
    transactions.findOneAndUpdate(conditions, update, options, callback);
}

// DELETE
exports.delete = function (req, res) {
    var conditions, callback, retObj



    conditions = {_id: req.params._id}
        , callback = function (err, doc) {
        retObj = {
            meta: {"action": "delete", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    }
    transactions.remove(conditions, callback);
}

