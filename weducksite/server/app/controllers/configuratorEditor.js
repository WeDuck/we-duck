/**
 * Created by Joost Rottger on 03-11-13.
 */

var mongoose = require('mongoose'),
    configEdit = mongoose.model('configuratorEditor');


// CREATE
exports.create = function (req, res) {
    var doc = new configEdit(req.body);
    console.log(doc);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);

    });
}

// RETRIEVE

// Retrieve all
exports.list = function (req, res) {
    var condition, fields, options;

    condition = {};
    fields = {};
    sort = {'modificationDate': -1};

    configEdit
        .find(condition, fields)
        .sort(sort)
        .exec(function (err, doc){
            var retObj = {
                meta: {"action": "list",'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}
// Retrieve one
exports.detail = function (req, res) {
    var condition, fields, options;

    condition = {_id: req.params._id},
        fields = {},
        options = {'createdAt': -1};

    configEdit
        .find(condition, fields)
        .exec(function (err, doc){
            var retObj = {
                meta: {"action": "list",'timestamp': new Date()},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        })
}

// UPDATE
exports.update = function (req, res) {

    var conditions =
        {_id: req.params._id}
        , update = {

        }
        , options = { multi: true }
        , callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };
    configEdit.findOneAndUpdate(conditions, update, options, callback);
}

// DELETE
exports.delete = function (req, res) {
    var conditions, callback, retObj;

    conditions = {_id: req.params._id}
        , callback = function (err, doc) {
        retObj = {
            meta: {"action": "delete", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    }
    configEdit.remove(conditions, callback);
}

//ADD COLOR
exports.addColor = function (req, res) {

    console.log('ADD COLOR TO CONFIGEDITOR.');

    var conditions =
        {_id: req.body.id}
        , update = {
            $push: {colorArray: req.body.color},
            safe: true
        }
        , options = { multi: false }
        , callback = function (err, doc) {
            configEdit.findByIdAndUpdate(req.body._id, update, function (err, me) {
                var retObj = {
                    meta: {"action": "update", 'timestamp': new Date()},
                    doc: me,
                    err: err
                };
                return res.send(retObj);
            });
        };

    configEdit.findOneAndUpdate(conditions, update, options, callback);
}
//ADD COLOR
exports.addPattern = function (req, res) {

    console.log('ADD PATTERN TO CONFIGEDITOR.');

    var conditions =
        {_id: req.body.id}
        , update = {
            $push: {patternArray: req.body.pattern},
            safe: true
        }
        , options = { multi: false }
        , callback = function (err, doc) {
            configEdit.findByIdAndUpdate(req.body._id, update, function (err, me) {
                var retObj = {
                    meta: {"action": "update", 'timestamp': new Date()},
                    doc: me,
                    err: err
                };
                return res.send(retObj);
            });
        };

    configEdit.findOneAndUpdate(conditions, update, options, callback);
}

