var mongoose = require('mongoose')
    , Contact = mongoose.model('Contact')

exports.sendQuestion = function (req) {
    var nodemailer = require("nodemailer");

    var smtpTransport = nodemailer.createTransport("SMTP");
    var mailTo = "linde_1994@hotmail.com, michaelwenting@live.nl";
    var name = req.body.name;
    var subject = req.body.subject;
    var email = req.body.mail;
    var question = req.body.question;

    var htmlStr = "Vraag van: " + name + "," +"<br/>" + "over: " + subject + "<br/>" + "<br/>" + "<i>" + question + "</i>" + "<br/>" + "<br/>" +
        "Gegevens klant: " + "<br/>" + "<b>" +
        name + "<br/>" + email;

    var mailOptions = {
        from: "weduck.nl <weDuck@info.nl>",
        to: mailTo,
        subject: "Vraag van: "+ name + ", onderwerp: " + subject,
        text: htmlStr,
        html: htmlStr
    };

    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + response.message);
        }
        smtpTransport.close();
    })
}
