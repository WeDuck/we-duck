var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schemaName = Schema({
    description: {type: String, required: true},
    colorArray: [],
    patternArray: [{url:String}],
    meta: {},
    modificationDate: {type: Date, "default": Date.now}
});

var modelName = "configuratorEditor";
var collectionName = "configuratorEditor";
mongoose.model(modelName, schemaName, collectionName);

