var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schemaName = Schema({
    color: {type: String},
    size: {type: Number, required: true},
    pattern: {type: String},
    volume: {type: String || ''},
    accessoires: {type: String },
    text: {type: String},
    image: {type: String},
    configImage: {type: String},
    meta: {},
    modificationDate: {type: Date, "default": Date.now}
});

var modelName = "Configurator";
var collectionName = "configurator";
mongoose.model(modelName, schemaName, collectionName);

