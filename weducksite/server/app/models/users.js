
var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var idConfigSchema = Schema({
    _idConfig: {type: Schema.Types.ObjectId}
});


var idTransactionSchema = Schema({
    _idTransaction: {type: Schema.Types.ObjectId}

});

var schemaName = Schema({
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    street: {type: String, required: true},
    zip: {type: String, required: true},
    state: {type: String, required: true},
    bankName: {type: String, required: true},
    number: {type: String, required: true},
    configurators:[idConfigSchema],
    transaction:[idTransactionSchema],
    meta: {},
    modificationDate: {type: Date, "default": Date.now}
});

schemaName.path('name').validate(function (val) {
    return (val !== undefined && val !== null && val.length >= 6);
}, 'Invalid name');

schemaName.path('email').validate(function (val) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(val);
}, 'Invalid email');

var modelName = "User";
var collectionName = "users";
mongoose.model(modelName, schemaName, collectionName);

