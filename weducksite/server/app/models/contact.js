var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schemaName = Schema({
    meta: {},
    modificationDate: {type: Date, "default": Date.now}
});

var modelName = "Contact";
var collectionName = "contact";
mongoose.model(modelName, schemaName, collectionName);

