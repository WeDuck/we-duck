var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var duckiesID = Schema({
    _id: {type: Schema.Types.ObjectId}
});
var schemaName = Schema({
    description: {type: String, required: true},
    duckies: [duckiesID],
    meta: {},
    modificationDate: {type: Date, "default": Date.now}
});

var modelName = "Wall";
var collectionName = "wallOfDuck";
mongoose.model(modelName, schemaName, collectionName);

