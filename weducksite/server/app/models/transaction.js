var mongoose;
mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var productID = Schema({
    _id: {type: Schema.Types.ObjectId}
});

var relationID = Schema({
    _id: {type: Schema.Types.ObjectId}
});

var schemaName = Schema({
    products:[productID],
    relations:[relationID],
    unitsOrdered: {type: Number},
    present: {type: String},
    price: {type: Number},
    delivered: {type: String},
    meta: {},
    modificationDate: {type: Date, "default": Date.now}
});

var modelName = "Transaction";
var collectionName = "transactions";
mongoose.model(modelName, schemaName, collectionName);

