module.exports = function (app) {

    var controller = require("../app/controllers/transaction.js");

    // CREATE
    app.post("/transactions", app.ensureAuthenticated, controller.create);

    // RETRIEVE
    app.get("/transactions", controller.list);
    app.get("/transactions/:_id",app.ensureAuthenticated, controller.detail);

    // UPDATE
    app.put("/transactions/:_id",app.ensureAuthenticated, controller.update);

    // DELETE
    app.delete("/transactions/:_id", app.ensureAuthenticated, controller.delete);

    //ADD IDS
    app.put("/transactions/:_id/addIDS", controller.addIDS);


}
