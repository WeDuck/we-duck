module.exports = function (app) {

    var controller = require('../app/controllers/users.js');
    // CREATE
    app.post('/users', controller.create);
    // RETRIEVE
    app.get('/users', controller.list);
    app.get('/users/:email', app.ensureAuthenticated, controller.detail);
    // UPDATE
    app.put('/users/:email', app.ensureAuthenticated, controller.update);
    // DELETE
    app.delete('/users/:email', app.ensureAuthenticated, controller.delete);
    // UPDATE PASSWORD
    app.put('/users/:email/password', controller.updatePassword);
    //SENT MAIL
    app.put('/users/:email/sendPassword', controller.sendPassword);
    //ADD DUCK
    app.put("/users/:email/addDuck", controller.updateUserWithDucks);
    //ADD TRANSACTION
    app.put("/users/:email/addTransaction", controller.updateUserWithTransaction);

}