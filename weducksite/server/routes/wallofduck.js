module.exports = function (app) {

    var controller = require('../app/controllers/wallofduck.js');
    // CREATE
    app.post('/wall', controller.create);
    // RETRIEVE
    app.get('/wall', controller.list);
    app.get('/wall/:_id', controller.detail);
    // UPDATE
    app.put('/wall/:_id', app.ensureAuthenticated, controller.update);
    // DELETE
    app.delete('/wall/:_id', app.ensureAuthenticated, controller.delete);
}