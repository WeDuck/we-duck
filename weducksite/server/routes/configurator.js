module.exports = function (app) {

    var controller = require("../app/controllers/configurator.js");

    // CREATE
    app.post("/configurator", controller.create);

    // RETRIEVE
    app.get("/configurator", controller.list);
    app.get("/configurator/:_id", controller.detail);

    // UPDATE
    app.put("/configurator/:_id", controller.update);

    // DELETE
    app.delete("/configurator/:_id", controller.delete);

}
