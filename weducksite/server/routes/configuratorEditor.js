module.exports = function (app) {

    var controller = require("../app/controllers/configuratorEditor.js");

    // CREATE
    app.post("/configEdit",app.ensureAuthenticated, controller.create);

    // RETRIEVE
    app.get("/configEdit", controller.list);
    app.get("/configEdit/:_id", controller.detail);

    // UPDATE
    app.put("/configEdit/:_id",app.ensureAuthenticated, controller.update);

    //ADD COLOR
    app.put("/configEdit/:_id/addColor",app.ensureAuthenticated, controller.addColor);

    //ADD Pattern
    app.put("/configEdit/:_id/addPattern",app.ensureAuthenticated, controller.addPattern);

    // DELETE
    app.delete("/configEdit/:_id",app.ensureAuthenticated, controller.delete);

}
