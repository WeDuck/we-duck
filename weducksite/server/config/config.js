module.exports = {
    development: {
        db: 'mongodb://localhost/P507388', // change p123456 with your database
        port: 43030,                        // change 3000 with your port number
        mailTo: "palla@hotmail.com",         // use your email address
        AccessControlAllowMethods: "GET,PUT,POST,DELETE",
        allowedDomains: "*"
    }, test: {

    }, production: {

    }

};