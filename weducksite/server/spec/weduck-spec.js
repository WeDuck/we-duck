var request = require('request');

/* VUL JE EIGEN URL IN, INLOG GEGEVENS*/
describe('We Duck', function(){
    beforeEach(function() {
        request.post({
            url: "http://autobay.tezzt.nl:43030/myLogin",
            form : {
                username: "joostrottger@hotmail.com",
                password: "viod24"
            }
        }, function(error, response, body) {
        });
    });
    
    it("Toevoegen van een bestaand user email adress, levert een MongoError op", function(done) {
        request.post({
            url: "http://autobay.tezzt.nl:43030/users",
            form : {
                name: "Michael Wenting",
                password: "michael19",
                email: "joostrottger@hotmail.com",
                street: "Doetinchemseweg 7",
                zip: "7006 AD Wehl",
                state: "Nederland",
                bankName: "Rabobank",
                number: "3883881"
            }
        }, function(error, response, body) {
            var json = JSON.parse(body);
            expect(json.err.name).toBe("MongoError");
            done();
        });
    });
    
    it("Haalt de gegevens van de user niet op als je niet bent ingelogd", function (done){
        request("http://autobay.tezzt.nl:43030/users/joostrottger@hotmail.com", function(error, response, body){
        var json = JSON.parse(body);
        expect(json.isVerified).toBe(false);
            done();
        });
    });
    
    
    it("Logs you out", function (done){
        request("http://autobay.tezzt.nl:43030/logout", function(error, response, body){
        var json = JSON.parse(body);
        expect(json.isVerified).toBe(false);
            done();
        });
    });
    
    it("Uitgelogd, de het account kan niet worden opgehaald", function (done){
        request("http://autobay.tezzt.nl:43030/account", function(error, response, body){
        var json = JSON.parse(body);
        expect(json.meta.description).toBe('Access is not allowed. Please login');
            done();
        });
    });
    
    it("Haalt gegevens configurator op ookal ben je niet ingelogd", function(done) {
        request("http://autobay.tezzt.nl:43030/configurator/52d938877487620850000006", function(error, response, body){
            var json = JSON.parse(body);
            expect(json.doc).not.toBeUndefined();
            expect(json.doc._id).toBe("52d938877487620850000006");
            done();
        });
    });
    
    it("Laat je een configurator aanmaken als je niet bent ingelogd", function(done) {
        request.post({
            url: "http://autobay.tezzt.nl:43030/configurator",
            form : {color: '#FFFFFF',
     				pattern: '',
     				volume: 'Badschuim',
     				text: '',
     				accessoires: 'duckie.obj',
     				image: '',
     				size: '5'
            }
        }, function(error, response, body) {
            var json = JSON.parse(body);
            expect(json.err).toBe(null);
            done();
        });
    });
    
    it("Haalt alle configurators op", function(done) {
        request("http://autobay.tezzt.nl:43030/configurator", function(error, response, body){
        var json = JSON.parse(body);
                    expect(json.doc).not.toBeUndefined();

            done();
        });
    });
    
    
    it("Haalt de gegevens van een configurator op", function(done) {
        request("http://autobay.tezzt.nl:43030/configurator/52dbb3ccc5dc668e36000007", function(error, response, body){
        var json = JSON.parse(body);
        expect(json.doc._id).toBe("52dbb3ccc5dc668e36000007");
            done();
        });
    });
    
    it("Haalt de duckies uit de wall of duck op, ookal ben je niet ingelogd", function(done) {
        request("http://autobay.tezzt.nl:43030/wall/52d5283e925db76945000006", function(error, response, body){
        var json = JSON.parse(body);
        expect(json.doc.duckies[1]).not.toBeUndefined();
        console.log(json.doc.duckies[1]);
            done();
        });
    });
    
    it("Haalt een transactie niet op als je niet bent ingelogd", function(done) {
        request("http://autobay.tezzt.nl:43030/transactions/52d3dbd2df681a3d2000000d", function(error, response, body){
        var json = JSON.parse(body);
		expect(json.isVerified).toBe(false);            
		done();
        });
    });
    
    
    
    

});