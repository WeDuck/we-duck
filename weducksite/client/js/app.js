//"use strict";
//console.log("hiyu");

/**
 * @see http://docs.angularjs.org/guide/concepts
 */
angular.module('myApp', [ 'myApp.services', 'myApp.directives'])
    .config(['$routeProvider', function ($routeProvider) {

        // Get home page
        $routeProvider.when('/home', {
            templateUrl: 'partials/home.html',
            controller: HomeCtrl
        });

        $routeProvider.when('/winkelmand/:_id', {
            templateUrl: 'partials/winkelmand-detail.html',
            controller: winkelmandDetailCtrl
        });

        // Get home page
        $routeProvider.when('/winkelmand', {
            templateUrl: 'partials/winkelmand.html',
            controller: winkelmandCtrl
        });

        // Get contact page
        $routeProvider.when('/contact', {
            templateUrl: 'partials/contact.html',
            controller: contactCtrl
        });

        // Get faq page
        $routeProvider.when('/faq', {
            templateUrl: 'partials/faq.html'
        });

        // Get bestelinfo page
        $routeProvider.when('/bestelinformatie', {
            templateUrl: 'partials/bestelinformatie.html'
        });


        // Get  configurators
        $routeProvider.when('/configurator', {
            templateUrl: 'partials/configurator-detail.html',
            controller: makeConfigurator
        });

        // Get  configurators
        $routeProvider.when('/configurator/:_id', {
            templateUrl: 'partials/configurator-detail.html',
            controller: makeConfiguratorDetail
        });

        //Get admin panel
        $routeProvider.when('/adminPanel', {
            templateUrl: 'partials/adminPanel.html',
            controller: adminPanelCtrl
        });



        // Get wallOfDuck page
        $routeProvider.when('/wall', {
            templateUrl: 'partials/wallofduck.html',
            controller: wallOfDuckCtrl
        });

        // Get product page
        $routeProvider.when('/transaction', {
            templateUrl: 'partials/transaction-list.html',
            controller: transactionListCtrl
        });

        // Get product page
        $routeProvider.when("/transaction/id/:_id", {
            templateUrl: 'partials/transaction-detail.html',
            controller: transactionDetailCtrl
        });

        // Get all users
        $routeProvider.when('/users', {
            templateUrl: 'partials/user-list.html',
            controller: UserListCtrl
        });
        // Get 1 user
        $routeProvider.when('/users/:email', {
            templateUrl: 'partials/user-detail.html',
            controller: getUserDetails
        });

        // When no valid route is provided
        $routeProvider.otherwise({
            redirectTo: "/home"
        });

    }])
