/*jslint node: true, plusplus: true */

"use strict";
/*Request om de gegevens van de klant op te halen op basis van het e-mailadres de transacties van de klant op te halen*/
function winkelmandCtrl($http, usersService, transactionService, configService, $scope) {
    $http({
        method: "GET",
        url: "/account"
    }).success(function (data) {
            var i;
            $scope.users = usersService.users.get({email: data.user.email });
            document.getElementById("getWinkelDetail").setAttribute("style", "display:block");
            document.getElementById("loginMelding").setAttribute("style", "display:none");
            $http({
                method: "GET",
                url: "/users/" + data.user.email
            }).success(function (data) {
                    for (i = 0; i < data.doc.transaction.length; i = i + 1) {
                        $http({
                            method: "GET", // haal de transacties op, opbasis van het de transacties die zich bevinden in de user.
                            url: "/transactions/" + data.doc.transaction[i]._id
                        }).success(function (data) {
                                if (data.doc.delivered !== "Ja") {
                                    var array = [data.doc._id];
                                    for (i = 0; i < array.length; i = i + 1) {

                                        $scope.transactions = transactionService.transactions.get({_id: array[i] });

                                    }


                                }
                            });
                    }
                });

        });


    $scope.getDetails = function (transactionID) {
        console.log("details: " + transactionID);
        window.location = "#/winkelmand/" + transactionID;
    }

}

/*haal de transactie opbasis van de routeparams, bij getDesign wordt een paramater doorgegeven en de gegevens van
* de configurator op gehaald die bij de transactie horen.*/
function winkelmandDetailCtrl($scope, configService, transactionService, $routeParams) {
    $scope.transactions = transactionService.transactions.get({_id: $routeParams._id });

    $scope.getDesign = function (duckId) {
        console.log("get design: " + duckId);
        $scope.configurator = configService.configurator.get({_id: duckId });
        document.getElementById("getDetailDuckList").setAttribute("style", "display:block");
    }


}


/*verstuurt een email naar we duck op basis van de naam van de klant en email adress van de klant
dit gebeurt doormiddel van een put request waarbij de naam, email , onderwerp en vraag van de klant wordt verstuurd.
 */
function contactCtrl($scope, $http) {
    $scope.contactForm = function (contactForm) {

        console.log("name: " + contactForm.nameCustomer + " email: " + contactForm.username + " onderwerp: " + contactForm.subject + "vraag: " + contactForm.question);

        $http({
            method: "PUT",
            url: "/contact/" + contactForm.username + "/sendQuestion",
            data: {"name": contactForm.nameCustomer, "mail": contactForm.username, "subject": contactForm.subject, "question": contactForm.question  }
        }).success(function () {
                console.log("email verstuurd: ");

            });
    }

}
/*bij geen bestaande user wordt de nieuwe user opgeslagen, bij een bestaande user worden de gegevens van deze
 * user opgehaald op basis van het route email */
function getUserDetails($location, $http, $scope, configService, usersService, $routeParams) {
    var isSuccess;
    $scope.users = usersService.users.get({email: $routeParams.email});


    $scope.banken = [
        "ABN AMRO", "Rabobank", "ING", "ASN Bank"
    ];


    if ($routeParams.email !== '0') {
        document.getElementById('passwordDiv').style.display = 'none';
        document.getElementById('inlogForm').style.display = 'none';

    }
    if ($routeParams.email === '0') {
        document.getElementById('changePasswordDiv').style.display = 'none';
        document.getElementById('getDuckList').style.display = 'none';
    }


    /* deze functie maakt het mogelijk de user te verwijderen*/
    $scope.delete = function () {
        usersService.users.delete({email: $routeParams.email});
        $location.path("/users");
    }

    /* deze functie bekijkt eerst over een $scope.users.doc._id aanwezig is, zo niet dan wordt een user opgeslagen als
    nieuwe user anders wordt de bestaande user upgedate
     */
    $scope.save = function (userForm) {
        if ($scope.users.doc && $scope.users.doc._id !== undefined) {
            usersService.users.update({email: $scope.users.doc.email}, $scope.users.doc, function (res) {
                isSuccess = res.err === null;
                $scope.isSuccess = isSuccess;
                if (isSuccess) {
                    $scope.msg = "User successfully updated.";
                } else {
                    $scope.err = res.err.message;
                    $scope.err = res.err.message;
                }
            });
        } else {
            console.log('Entering save', $scope.users.doc);
            usersService.users.save({}, $scope.users.doc, function (res) {
                isSuccess = res.err === null;
                if (isSuccess) {
                    $scope.msg = "User successfully created.";
                    document.getElementById("successUser").setAttribute("style", "display:block");
                } else {
                    $scope.err = res.err.message;
                    $scope.err = res.err.message;
                    document.getElementById("wrongUser").setAttribute("style", "display:block");
                }
            });
        }
    }


/*In deze functie kan het wachtwoord van de user worden veranderd, het nieuwe en oude wachtwoord wat de user intypt
* wordt uit het changeForm gehaald, daarna wordt eerst gekeken of de data die de user invult klopt doormiddel van een
* /myLogin waarbij het emailadress en ingetypte wachtwoord wordt meegegeven. Daarna wordt gekeken of het nieuwe
* wachtwoord en confirm wachtwoord hetzelfde zijn, als dit klopt wordt een put uitgevoerd.*/
    $scope.changePassword = function (changeForm) {
        var email = $routeParams.email;
        console.log("email uit het form: " + $scope.users.doc.email);

        var oldPassword = changeForm.oldPassword;
        var newPassword = changeForm.newPassword;
        var confirmPassword = changeForm.confirmPassword;
        console.log("old " + changeForm.oldPassword.length);
        $http({
            method: "POST",
            url: "/myLogin",
            data: {"username": $scope.users.doc.email, "password": oldPassword}
        }).success(function (data) {
                var isVerified = data.isVerified;
                if (isVerified === true) {
                    if (newPassword !== undefined && confirmPassword !== undefined) {
                        console.log("wachtwoorden ingevuld check");
                    } else {
                        console.log("ww zijn niet ingevuld");
                    }
                    if (newPassword === confirmPassword && changeForm.newPassword.length > 1 && changeForm.confirmPassword.length > 1) {
                        console.log("change ww");
                        document.getElementById("successUser").setAttribute("style", "display:block");

                        $http({
                            method: "PUT",
                            url: "/users/" + $scope.users.doc.email + "/password",
                            data: {"password": confirmPassword}
                        }).success(function (data) {
                                if (data.isVerified === true) {
                                    console.log("new " + $scope.users.doc.password);

                                }
                            });

                    } else {
                        console.log("ww komen niet overeen of voldoen niet");

                    }

                } else {
                    console.log("oude wachtwoord komt niet overeen");
                }


                if (isVerified === true) {
                    console.log("bingo");
                    if (newPassword !== null && confirmPassword !== null && newPassword === confirmPassword) {
                        console.log("new password= " + confirmPassword);


                    }
                    else {
                        console.log("nieuwe wachtwoorden komen niet overeen");
                    }
                } else {
                    console.log("Het huidige wachtwoord is niet correct");
                }
            });
    }

    $scope.getDetails = function (id) {
        var id = id;
        $scope.configurator = configService.configurator.get({_id: id});

        console.log('id ' + id);
        document.getElementById('getDetailDuckList').setAttribute('style', 'display:block;');
        document.getElementById('yourDucks').style.display = 'none';
        document.getElementById('getDuckList').style.display = 'none';
    }

    $scope.getBack = function () {
        document.getElementById('getDetailDuckList').setAttribute('style', 'display:none;');
        document.getElementById('getDuckList').setAttribute('style', 'display:block;');
        document.getElementById('yourDucks').setAttribute('style', 'display:block;');
    }

    $scope.getEdit = function (duckId) {
        console.log("Edit duckieee" + duckId);
        console.log($scope.configurator.doc._id)
        $location.path("/configurator/" + duckId);
    }
    $scope.makeOrder = function (id) {
        console.log("test");
        window.location = "#/transaction/id/" + id;

    }
}
/*Uitbreidingsmogelijkheid tot het maken van een adminPanel */
function adminPanelCtrl($http, $scope, configService) {
    var i, wallId, configEditorId;
    console.log("AdminPanel- under construction");

    $scope.configurator = configService.configurator.get();

    //Voeg een kleur toe aan configuratie
    $scope.addColor = function (colorForm) {
        var i;
        console.log("add colorform: ", colorForm.newColor);
        configEditorId = "52c876701c30be3f4f000004";

        $http({
            method: "GET",
            url: "/configEdit/" + configEditorId
        }).success(function (data) {
                console.log("lengte: " + data.doc.colorArray.length);
                var stopLoop = false;
                for (i = 0; i < data.doc.colorArray.length; i = i + 1) {
                    if (stopLoop === false) {
                        if (colorForm.newColor !== data.doc.colorArray[i]) {
                            console.log("i: " + i);
                            if (i === data.doc.colorArray.length - 1) {
                                $http({
                                    method: "PUT",
                                    url: "/configEdit/" + configEditorId + "/addColor",
                                    data: {"id": configEditorId, "color": colorForm.newColor}
                                }).success(function () {
                                        console.log("color added: " + colorForm.newColor);
                                    });
                            }
                        } else {
                            stopLoop = true;
                            console.log(" already: " + colorForm.newColor);

                        }
                    }
                }

            });
    }

    //Voeg een pattern toe aan configuratie
    $scope.addPattern = function (patternForm) {
        //console.log("add patterform: ", patternForm.newPattern);
        configEditorId = "52c876701c30be3f4f000004";

        $http({
            method: "PUT",
            url: "/configEdit/" + configEditorId + "/addPattern",
            data: {"id": configEditorId, "pattern": patternForm.newPattern}
        }).success(function () {
                console.log("pattern added: " + patternForm.newPattern);
            });

    }

    //Voeg een duck toe aan de duck of wall
    $scope.putToWall = function (configId) {

        wallId = "52c985cffb13748441000019";
        $http({
            method: "GET",
            url: "/wall/" + wallId
        }).success(function (data) {
                console.log("lengte: " + data.doc.duckies.length);
                if (data.doc.duckies.length === 0) {
                    $http({
                        method: "PUT",
                        url: "/wall/" + wallId,
                        data: {"duckiesId": configId}
                    }).success(function () {
                            console.log("duck added: " + configId);
                        });
                }

                var stopLoop = false;
                for (var i = 0; i < data.doc.duckies.length; i = i + 1) {
                    if (stopLoop === false) {
                        if (configId !== data.doc.duckies[i]._id) {
                            console.log("i: " + i);
                            if (i === data.doc.duckies.length - 1) {
                                $http({
                                    method: "PUT",
                                    url: "/wall/" + wallId,
                                    data: {"duckiesId": configId}
                                }).success(function () {
                                        console.log("duck added: " + configId);
                                    });
                            }
                        } else {
                            stopLoop = true;
                            console.log(" already: " + configId);
                        }

                    }
                }

            });
    }


}
/*In deze functie worden de duckies die zich in de array duckies binnen wall of duck bevinden opgehaald,
* deze duckies kunnen worden getoond in de wall of duck*/
function wallOfDuckCtrl($scope, wallService, $location) {
    var wallId = "52c985cffb13748441000019";
    console.log("wall of duck - under construction");
    $scope.wallofduck = wallService.wallofduck.get({_id: wallId});

    /*mogelijkheid tot het krijgen van de opmaak van een duck uit de wall of duck*/
    $scope.getDesign = function (duckId) {
        console.log("Get design of: " + duckId);

        $location.path("/configurator/" + duckId);

    }

    var items, link, images, i, page = 0, numPag = 0;


    /* haalt de images uit setOptions.js*/
    images = setOptions.image();
    link  = setOptions.link();
    createItems();
    scrollButtons();

    /* maakt <div> elementen aan, er wordt bepaald welke images uit de array images wordt
     *  gehaald gebruikt. Het hebben van meedere pagina's wordt hier berekend */
    function createItems() {
        var div, gallery, img, firstItem, lastItem;
        gallery = document.getElementById("gallery");
        console.log("create items");
        numPag = Math.ceil(images.length / 6) - 1;
        if (images.length > 6) {
            console.log(numPag);
            firstItem = page * 6;
            lastItem = firstItem + 6;

        }
        for (i = firstItem; i <= lastItem; i = i + 1) {
            var href = document.createElement("a");
            href.setAttribute('href', "#/configurator/" + link[i]);
            div = document.createElement("div");
            div.setAttribute('class', 'galleryItem');
            div.setAttribute('id', 'galleryItem' + i);
            img = document.createElement("img");
            img.setAttribute('src', 'images/' + images[i]);
            div.appendChild(img);
            href.appendChild(div);
            gallery.appendChild(href);
        }
        scrollNav();
    }
    /* verwijderd alle items die aanwezig zijn */
    function removeItems() {
        var items;
        console.log("remove items");
        items = document.getElementsByClassName("galleryItem");
        for (i = 0; i < items.length; i) {
            items[i].parentNode.removeChild(items[i]);
        }
    }
    /*geeft alle (2) scrollButtons de eventhandler onmousedown */
    function scrollButtons() {
        var buttons;
        buttons = document.getElementsByClassName("scrollButton");
        for (i = 0; i < buttons.length; i = i + 1) {
            buttons[i].onmousedown = scrollButtonClick(buttons[i]);
        }


    }
    /* handelt het clickevent op een scrollButton af. De nieuwe paginanummers worden bepaald, alle items
     * en de navigatie onderaan worden verwijderd en opnieuw toegevoegd met de nieuwe gegevens.
     */
    function scrollButtonClick(button) {
        return function (event) {
            numPag = parseInt(numPag, 10);
            if (button.id === "scrollLeft") {
                page = page - 1;
            } else {
                page = page + 1;
            }
            console.log(numPag);
            if (page < 0) {
                page = numPag;
            }
            if (page > numPag) {
                page = 0;
            }
            console.log(page);
            removeNav();
            removeItems();
            createItems();
        };
    }
    /* Zorgt dat de navigatiestippen onderaan de juiste image hebben */
    function scrollNav() {

        var img, dot, scrollNav = document.getElementById("scrollNav");
        for (i = 0; i <= numPag; i = i + 1) {
            if (i === page) {
                dot = "dot2.png";
            } else {
                dot = "dot.png";
            }
            img = document.createElement("img");
            img.setAttribute('class', 'scrollNavButton');
            img.setAttribute('src', 'images/' + dot);
            scrollNav.appendChild(img);
        }

    }
    /* verwijderd alle naviagtiestippen */
    function removeNav() {
        console.log("removenav");
        var scrollNav = document.getElementsByClassName("scrollNavButton");
        console.log(scrollNav);
        for (i = 0; i < scrollNav.length; i) {
            scrollNav[i].parentNode.removeChild(scrollNav[i]);
        }
    }
}

function transactionListCtrl() {
    console.log("transactionListCtrl - under construction");

}
/* De gegevens voor het afronden van de bestelling worden hier opgehaald */
function transactionDetailCtrl($scope, $http, $location, usersService, configService, transactionService, $routeParams) {
    var isSuccess, price, input, select, units, present;

    $scope.configurator = configService.configurator.get({id: $routeParams._id});

    $scope.present = [
        "Ja", "Nee"
    ];
    $scope.banken = [
        "ABN AMRO", "Rabobank", "ING", "ASN Bank"
    ];

    $scope.present = [
        "Nee", "Ja (+ € 1,99)"
    ];

    //Haalt gegevens van ingelogde gebruiker op
    $http({
        method: "GET",
        url: "/account"
    }).success(function (data) {
            $scope.users = usersService.users.get({email: data.user.email});
        });
    $scope.delete = function () {
        transactionService.transactions.delete({id: $routeParams._id});
        $location.path("/configurator");
    }
    console.log("route" + $routeParams._id)

    $scope.configurator = configService.configurator.get({_id: $routeParams._id});

    //update user op basis van het email adres
    $scope.updateUser = function () {
        console.log("update");
        var email = $scope.users.doc.email;
        var name = $scope.users.doc.name;
        var street = $scope.users.doc.street;
        var zip = $scope.users.doc.zip;
        var state = $scope.users.doc.state;
        var bankName = $scope.users.doc.bankName;
        var reknumber = $scope.users.doc.number;


        $http({
            method: "PUT",
            url: "/users/" + $scope.users.doc.email,
            data: {"email": email, "name": name, "street": street, "zip": zip, "state": state, "bankName": bankName, "number": reknumber}
        }).success(function () {
                console.log("update user: " + $scope.users.doc.name);
            });
    }
    /*Berekend de prijs van de aankoop */
    $scope.price = function () {
        console.log("size eend: " + $scope.configurator.doc.size);

        input = document.getElementById("input");
        select = document.getElementById("select")
        price = $scope.configurator.doc.size + 2.99;
        units = input.value;
        present = select.value;
        console.log(present);
        input = input.value * price;
        select = select.value * 1.99;
        console.log(price, input, select);
        price = input + select;
        console.log(price);
        document.getElementById('totalPrice').innerHTML = '<br>' + 'Totale Prijs: € ' + price.toFixed(2);
    }

    /* voegt de bestelling toe aan de database*/
    $scope.bestel = function () {
        var delivered = "Nee";
        console.log("aantal eendjes: " + units + ", wel of geen cadeau: " + present);

        console.log("relatie: " + $scope.users.doc._id);
        console.log("productid: " + $routeParams._id);

        $http({
            method: "POST",
            url: "/transactions",
            data: {"units": units, "present": present, "price": price, "delivered": delivered }
        }).success(function (res) {
                console.log("new order!");
                var id = $scope.get = res.doc._id;

                $http({
                    method: "PUT",
                    url: "/transactions/" + id + "/addIDS",
                    data: {"transactionId": id, "relationID": $scope.users.doc._id, "productID": $routeParams._id}
                }).success(function () {
                        // window.location = "#/winkelmand"

                        $http({
                            method: "PUT",
                            url: "/users/" + $scope.users.doc.email + "/addTransaction",
                            data: {"transactionID": id}
                        }).success(function () {


                            });
                    });
            });

    }
}
/* Hier wordt de configurator  gedefineerd en gemaakt doormiddel van de functies, waarbij een bestaande configurator wordt aangeroepen doormiddel van het route parameter */
function makeConfiguratorDetail($http, usersService, configService, $routeParams, $scope) {
    var duckSize, bCanPreview, canvas, hexColor, duckAccessoires, duckColor, duckVolume, isSuccess, duckPattern, color, cld, i, block, viewer, colors = [], patterns = [], sizes = [], accessoires = [], setItem, setColor = "#FFFFFF";
    console.log("get duck and updateeeee");

    $scope.configurator = configService.configurator.get({_id: $routeParams._id});

    $http({
        method: "GET",
        url: "/account"
    }).success(function (data) {
            $scope.users = usersService.users.get({email: data.user.email});
        });

    // Laad een bestaande eend als die er is
    $http({
        method: "GET",
        url: "/configurator/" + $routeParams._id
    }).success(function (data) {
            console.log("get duckie " + data.doc.accessoires);
            console.log("get color " + data.doc.color);
            console.log("get size " + data.doc.size);
            console.log("get volume " + data.doc.volume);
            console.log("get pattern " + data.doc.pattern);
            console.log("get tekst " + data.doc.text);
            console.log("get image" + data.doc.image);

            duckAccessoires = data.doc.accessoires;
            duckSize = data.doc.size;
            duckVolume = data.doc.volume;
            duckPattern = data.doc.pattern;
            duckColor = data.doc.color;

            /* De viewer voor het 3D-model wordt ingesteld */
            viewer = new JSC3D.Viewer(document.getElementById('canvas'));
            viewer.setParameter('SceneUrl', '../duckie/' + duckAccessoires);
            viewer.setParameter('ModelColor', '#FFFFFF');
            viewer.setParameter('BackgroundColor1', '#FFFFFF');
            viewer.setParameter('BackgroundColor2', '#FFFFFF');
            viewer.setParameter('RenderMode', 'textureSmooth');
            viewer.setParameter('setDefinition', 'high');

            viewer.init();
            viewer.onloadingcomplete = function () {
                getBeginColor();
                getBeginPattern();

            };

            function getBeginColor() {
                var mesh = viewer.getScene().getChildren()[1];
                mesh.setMaterial(new JSC3D.Material('', 0, duckColor, 0, true));
            }

            function getBeginPattern() {
                var newTex = new JSC3D.Texture('blackandwhite');
                cld = viewer.getScene().getChildren();
                newTex.onready = function () {
                    cld[1].setTexture(this);
                    viewer.update();
                };
                newTex.createFromUrl('patterns/' + duckPattern, false);
            }


            /* Veranderd de kleur van de badeend in de gekozen kleur, die in de variabele color zit */
            function changeColor(x, y, button) {
                color = formatColor();
                duckColor = formatColor();

                console.log("huidige kleur: " + color);
                if (button === 0) {
                    var newMat;
                    cld = viewer.getScene().getChildren();
                    for (i = 0; i < cld.length; i = i + 1) {
                        newMat = new JSC3D.Material('', 0, color, 0, true);
                        if (i === 1) {
                            cld[i].setMaterial(newMat);
                        }
                    }
                    viewer.update();
                }
            }

            /*Veranderd het patroon van de badeend */
            function changePattern(x, y, button, pattern) {
                var newTex = new JSC3D.Texture('blackandwhite');
                cld = viewer.getScene().getChildren();
                newTex.onready = function () {
                    cld[1].setTexture(this);
                    viewer.update();
                };
                duckPattern = pattern;
                newTex.createFromUrl('patterns/' + pattern, false);
            }

            /*Veranderd een #-kleurcode naar een 0x-kleurcode */
            function formatColor() {
                var color;
                color = getColor();
                color = color.substring(1, 7);
                color = "0x" + color;
                return color;
            }

            /* In de onderstaande reeks functies wordt de configurator aan gemaakt*/
            var blocks = [];
            colors = setOptions.color();
            patterns = setOptions.pattern();
            sizes = setOptions.size();
            accessoires = setOptions.accessoire();

            for (i = 0; i < 9; i = i + 1) {
                blocks[i] = document.getElementById("item" + i);
                block = document.getElementById("item" + i);
                block.onmousedown = mouseClick(blocks, block);

            }
            iconEventHandlers();

            function getColor() {
                return setColor;
            }

            /* Handelt de muisklik op een block (=grote iconen in de configurator) en kruisje af */
            function mouseClick(div, block) {
                return function (event) {
                    var selectionBox = document.getElementsByClassName("selectionBox");
                    if (selectionBox.length === 0) {
                        setItem = block.id;

                        if (block.id === "item7") {
                            $scope.update();
                        }
                        else if (block.id === "item6") {
                            $scope.product();
                        } else {
                            mouseClickFirst(div);
                        }
                    } else {
                        if (div.id === "closeButton") {
                            mouseClickCloseButton(selectionBox);
                        }


                    }

                };
            }

            /* Deze functie wordt aangeroepen door mouseClick en roept alle functies aan die nodig
             * zijn om het optiepaneel te maken aan
             */
            function mouseClickFirst(blocks) {
                createBlock();
                hideItems(blocks);
                createClose();
                createOption();
            }

            /* Doet het tegenovergestelde van bovenstaande functie */
            function mouseClickCloseButton() {
                removeClose();
                closeBlock();
                showItems();
                removeOption();
            }

            /* Wordt aangeroepen als er op een kleur geklikt wordt. Het veranderd de ingestelde (setColor)
             * kleur en de kleur van de badeend door de functie changeColor aan te roepen
             */
            function mouseClickOptionColor(option) {
                return function (event) {
                    var idNum = option.id.substring(6, 8), color;
                    color = colors[idNum];
                    setColor = color;
                    changeColor(event.x, event.y, event.button);
                };
            }

            /* Roept changePattern aan wanneer er op een patroon geklikt wordt */
            function mouseClickOptionPattern(pattern) {
                return function (event) {
                    console.log(pattern);
                    changePattern(event.x, event.y, event.button, pattern);
                };
            }

            /* Wijzigt ingestelde inhoud wanneer er op een inhoud geklikt wordt */
            function mouseClickOptionVolume(div) {
                return function (event) {

                    console.log("div: " + div.id);

                    if (div.id === "option0") {
                        duckVolume = "kindvriendelijke shampoo";
                    }
                    if (div.id === "option1") {
                        duckVolume = "Bad olie";
                    }
                    if (div.id === "option2") {
                        duckVolume = "Shampoo";
                    }
                    if (div.id === "option3") {
                        duckVolume = "Badzout";
                    }
                };
            }

            /* Wijzigt ingestelde accessoire wanneer er op een accessoire geklikt wordt */
            function mouseClickOptionAccessories(div) {
                return function (event) {
                    if (div.id === "option0") {
                        duckAccessoires = "duckiewithhat.obj";
                        console.log("duckiewithhat");
                        viewer.setParameter('SceneUrl', '../duckie/duckiewithhat.obj');
                        viewer.init();

                    }
                    if (div.id === "option1") {
                        duckAccessoires = "duckiewithheart.obj";
                        console.log("duckiewithheart");
                        viewer.setParameter('SceneUrl', '../duckie/duckiewithheart.obj');
                        viewer.init();
                    }
                    if (div.id === "option2") {
                        console.log("evil");
                        duckAccessoires = "duckieevil.obj";
                        viewer.setParameter('SceneUrl', '../duckie/duckieevil.obj');
                        viewer.init();
                    }
                    if (div.id === "option3") {
                        console.log("duckie");
                        duckAccessoires = "duckie.obj";
                        viewer.setParameter('SceneUrl', '../duckie/duckie.obj');
                        viewer.init();
                    }
                }
            }

            /* Wijzigt ingestelde formaat wanneer er op een formaat geklikt wordt */
            function mouseClickOptionSize(div) {
                return function (event) {
                    if (div.id === "option0") {
                        console.log("10 cm");
                        duckSize = 10;
                    }
                    if (div.id === "option1") {
                        console.log("5 cm");
                        duckSize = 5;
                    }
                    if (div.id === "option2") {
                        console.log("20 cm");
                        duckSize = 20;
                    }
                    if (div.id === "option3") {
                        console.log("15 cm");
                        duckSize = 15;
                    }

                }
            }

            /* Een reeks aan functies die hieronder staan spreken voorzich. Ze maken elementen aan of
             * verwijderen ze. Geldt voor alle functies tot aan het volgende commentaar
             */
            function hideItems(blocks) {
                for (i = 0; i < blocks.length; i = i + 1) {
                    blocks[i].setAttribute('class', 'icon hidden');
                    blocks[i].parentNode.setAttribute('class', 'item hidden');
                }
            }

            function showItems() {
                var icons = document.getElementsByClassName("icon hidden"),
                    items = document.getElementsByClassName("item hidden");

                for (i = 0; i < items.length; i) {
                    items[i].setAttribute('class', 'item');
                    icons[i].setAttribute('class', 'icon');
                }
            }

            function createBlock() {
                var div, square;
                div = document.createElement("div");
                div.setAttribute('class', 'selectionBox');
                square = document.getElementById("squareRight");
                square.appendChild(div);
            }

            function closeBlock() {
                var div = document.getElementsByClassName("selectionBox");
                div[0].parentNode.removeChild(div[0]);
            }

            function createClose() {
                var div = document.createElement("div"),
                    bar = document.createElement("div"),
                    block = document.getElementsByClassName("selectionBox")[0],
                    img = document.createElement("img");
                img.setAttribute('class', 'closeButton');
                img.setAttribute('id', 'closeButton');
                img.setAttribute('src', 'images/close.png');
                img.onmousedown = mouseClick(img, img);
                bar.setAttribute('class', 'bar');
                bar.setAttribute('id', 'barCloseButton');
                bar.appendChild(img);
                block.appendChild(bar);
            }

            function removeClose() {
                var div = document.getElementsByClassName("selectionBox"),
                    bar = document.getElementById("barCloseButton");
                div[0].removeChild(bar);
            }

            function createOption() {
                var div, div2, square, colorPalette;
                square = document.getElementsByClassName("selectionBox")[0];
                div = document.createElement("div");
                div2 = document.createElement("div");
                colorPalette = document.createElement("div");
                colorPalette.setAttribute('class', 'colorPalette');
                colorPalette.setAttribute('id', 'colorPalette');
                if (setItem === "item2") {                  //colors

                    for (i = 0; i < 15; i = i + 1) {
                        div = document.createElement("div");
                        div.setAttribute('class', 'option');
                        div.setAttribute('id', 'option' + i);
                        div.setAttribute('style', 'background-color:' + colors[i]);
                        div.onmousedown = mouseClickOptionColor(div);
                        colorPalette.appendChild(div);
                    }

                    div = document.createElement("div");
                    div.setAttribute('class', 'moreColors');
                    div.setAttribute('id', 'moreColors');
                    div.onmousedown = preview();
                    div.innerHTML = "Meer kleuren!";
                    square.appendChild(colorPalette);
                    square.appendChild(div);
                }

                if (setItem === "item1") {                  //pattern
                    for (i = 0; i < 16; i = i + 1) {
                        div = document.createElement("div");
                        div.setAttribute('class', 'option');
                        div.setAttribute('id', 'option' + i);
                        div.setAttribute('style', 'background-image: url(\'patterns/' + patterns[i] + '\'); background-size: contain');
                        div.onmousedown = mouseClickOptionPattern(patterns[i]);
                        colorPalette.appendChild(div);

                    }
                    square.appendChild(colorPalette);
                }
                if (setItem === "item3") {                  //volume
                    for (i = 0; i < 4; i = i + 1) {
                        var img = document.createElement("img");
                        div = document.createElement("div");
                        div.setAttribute('class', 'optionSize');
                        div.setAttribute('id', 'option' + i);
                        img.setAttribute('src', 'images/' + filling[i]);
                        div.onmousedown = mouseClickOptionVolume(div);
                        div.appendChild(img);
                        colorPalette.appendChild(div);

                    }
                    square.appendChild(colorPalette);
                }
                if (setItem === "item4") {                  //accessoires
                    for (i = 0; i < 4; i = i + 1) {
                        var img = document.createElement("img");
                        div = document.createElement("div");
                        div.setAttribute('class', 'optionSize');
                        div.setAttribute('id', 'option' + i);
                        img.setAttribute('src', 'images/' + accessoires[i]);
                        div.onmousedown = mouseClickOptionAccessories(div);
                        div.appendChild(img);
                        colorPalette.appendChild(div);

                    }
                    square.appendChild(colorPalette);
                }

                if (setItem === "item5") {                  //accessoires
                    var div = document.createElement("div");
                    var input = document.createElement("input");

                    div.appendChild(input);
                    colorPalette.appendChild(div);

                }
                square.appendChild(colorPalette);


                if (setItem === "item8") {                  //grootte
                    for (i = 0; i < 4; i = i + 1) {
                        img = document.createElement("img");
                        div = document.createElement("div");
                        div.setAttribute('class', 'optionSize');
                        div.setAttribute('id', 'option' + i);
                        img.setAttribute('src', 'images/' + sizes[i]);
                        div.onmousedown = mouseClickOptionSize(div);
                        div.appendChild(img);
                        colorPalette.appendChild(div);

                    }
                    square.appendChild(colorPalette);
                }

            }

            function removeOption() {
                var button, square;
                //options = document.getElementsByClassName("option");
                button = document.getElementById("moreColors");
                square = document.getElementsByClassName("colorPalette")[0];
                //for (i = 0; i < options.length - 1; i) {
                square.parentNode.removeChild(button);
                square.parentNode.removeChild(square);

                //}
            }

            /* Geeft de afbeelding voor de iconen in het keuzepaneel eventhandlers */
            function iconEventHandlers() {
                var icon;
                for (i = 0; i < 9; i = i + 1) {
                    icon = document.getElementById("item" + i);
                    icon.onmouseover = changeIconToGif(icon);
                    icon.onmouseout = changeIconToJpg(icon);
                }

            }

            /* Bij een mouseover veranderd de JPG in een GIF */
            function changeIconToGif(icon) {
                return function () {
                    icon.src = icon.src.substr(0, icon.src.length - 3) + "gif";

                };
            }

            /* Bij een mouseout veranderd de GIF in een JPG */
            function changeIconToJpg(icon) {
                return function () {
                    icon.src = icon.src.substr(0, icon.src.length - 3) + "jpg";
                };
            }

            /*Instellingen voor het "meer kleuren"-scherm. */
            function preview() {
                return function () {
                    var div, canvas, square, colorpicker;
                    square = document.getElementsByClassName("selectionBox")[0];

                    colorpicker = document.getElementsByClassName('colorpicker');
                    if (colorpicker.length === 0) {
                        div = document.createElement("div");
                        div.setAttribute('class', 'colorpicker');
                        div.setAttribute('style', 'display:none');

                        canvas = document.createElement("canvas");
                        canvas.setAttribute('id', 'picker');
                        canvas.setAttribute('var', '3');
                        canvas.setAttribute('style', 'width="300px" height="300px"');
                        canvas.onmousedown = 'setColor()';
                        div.appendChild(canvas);
                        square.appendChild(div);
                        div = document.createElement("div");
                        div.setAttribute('class', 'selectedColor');
                        div.setAttribute('id', 'selectedColor');
                        div.onmousedown = preview();
                        square.appendChild(div);
                    }
                    previewEvent();
                    createBack();
                };
            }

            function previewEvent() {
                removeOption();
                setPreview();
            }

            /* Veranderd de geselecteerde (setColor) kleur in de kleur die is aangeklikt in het kleurenwiel */
            function changeColorColorwheel(color) {
                setColor = color;
            }

            /* Maakt een "vorige"-pijltje aan, die het kleurenwiel sluit en terug keert naar optiepaneel */
            function createBack() {
                var img = document.createElement("img"),
                    bar = document.getElementById("barCloseButton"),
                    back = document.getElementsByClassName("backButton");
                if (back.length === 0) {
                    img.setAttribute('class', 'backButton');
                    img.setAttribute('id', 'backButton');
                    img.setAttribute('src', 'images/back.png');
                    img.onmousedown = removeBack();
                    bar.appendChild(img);
                }
            }

            /* Verwijderd het "vorige"-pijltje */
            function removeBack() {
                return function () {
                    var div = document.getElementById("backButton"),
                        bar = document.getElementById("barCloseButton"),
                        color = document.getElementById("selectedColor"),
                        canvas = document.getElementsByClassName("colorpicker")[0];
                    bar.removeChild(div);
                    bar.parentNode.removeChild(color);
                    bar.parentNode.removeChild(canvas);
                    createOption();
                    setColor = getColor();
                    changeColor(0, 0, 0);
                };
            }

            function setPreview() {
                bCanPreview = true;
                canvas = document.getElementsByClassName('colorpicker')[0];
                canvas.setAttribute('style', 'display:block');
            }

            function setColor() {
                return function () {
                    console.log("CHECK!!!");
                    setColor = hexColor;
                    $('#option15').css('backgroundColor', setColor);
                };
            }

            function getColor() {
                return setColor;
            }

        });

    /* Slaat het geconfigureerde eendje op in de database */
    $scope.update = function () {

        console.log("update duckiee" + $routeParams._id);
        console.log("test: " + duckColor);

        $http({
            method: "PUT",
            url: "/configurator/" + $routeParams._id,
            data: {"size": duckSize, "volume": duckVolume, "pattern": duckPattern, "accessoires": duckAccessoires, "color": duckColor}
        }).success(function () {
                console.log("duck upgedate");

            });


    }

    /* Haalt de transactie op die bij het ontworpen eendje */
    $scope.product = function () {
        $http({
            method: "GET",
            url: "/configurator/" + $routeParams._id
        }).success(function (data) {
                console.log("test", data.doc._id);
                console.log("make order to: " + $scope.users.doc._id);
                window.location = "#/transaction/id/" + data.doc._id;

            });

    }
}

/* De onderstaande functie is al beschreven in de bovenstaande functie */
function makeConfigurator($http, $routeParams, $scope, usersService, configService) {
    var duckSize, bCanPreview, filling, canvas, hexColor, duckAccessoires, duckVolume, isSuccess, duckPattern, color, cld, i, block, viewer, colors = [], patterns = [], sizes = [], accessoires = [], setItem, setColor = "#FFFFFF";


    // LOAD DUCK
    console.log("Check: ", getColor());
    duckAccessoires = 'duckie.obj';

    viewer = new JSC3D.Viewer(document.getElementById('canvas'));
    viewer.setParameter('SceneUrl', '../duckie/' + duckAccessoires);
    viewer.setParameter('ModelColor', '#FFFFFF');
    viewer.setParameter('BackgroundColor1', '#FFFFFF');
    viewer.setParameter('BackgroundColor2', '#FFFFFF');
    viewer.setParameter('RenderMode', 'textureSmooth');
    viewer.setParameter('setDefinition', 'high');

    viewer.init();

    function changeColor(x, y, button) {
        color = formatColor();
        console.log("huidige kleur: " + color);
        if (button === 0) {
            var newMat;
            cld = viewer.getScene().getChildren();
            for (i = 0; i < cld.length; i = i + 1) {
                newMat = new JSC3D.Material('', 0, color, 0, true);
                if (i === 1) {
                    cld[i].setMaterial(newMat);
                }
            }
            viewer.update();
        }
    }

    function changePattern(x, y, button, pattern) {
        var newTex = new JSC3D.Texture('blackandwhite');
        cld = viewer.getScene().getChildren();
        newTex.onready = function () {
            cld[1].setTexture(this);
            viewer.update();
        };
        duckPattern = pattern;
        newTex.createFromUrl('patterns/' + pattern, false);
    }

    function formatColor() {
        var color;
        color = getColor();
        color = color.substring(1, 7);
        color = "0x" + color;
        return color;
    }

    // CREATE CONFIG
    var blocks = [];
    colors = setOptions.color();
    patterns = setOptions.pattern();
    sizes = setOptions.size();
    accessoires = setOptions.accessoire();
    filling = setOptions.filling();

    for (i = 0; i < 9; i = i + 1) {
        blocks[i] = document.getElementById("item" + i);
        block = document.getElementById("item" + i);
        block.onmousedown = mouseClick(blocks, block);

    }
    iconEventHandlers();

    function getColor() {
        return setColor;
    }

    function mouseClick(div, block) {
        return function (event) {
            var selectionBox = document.getElementsByClassName("selectionBox");
            if (selectionBox.length === 0) {
                setItem = block.id;

                if (block.id === "item7") {
                    $scope.save();
                }
                else if (block.id === "item6") {
                    $scope.product();
                } else {
                    mouseClickFirst(div);
                }
            } else {
                if (div.id === "closeButton") {
                    mouseClickCloseButton(selectionBox);
                }


            }

        };
    }

    function mouseClickFirst(blocks) {
        createBlock();
        hideItems(blocks);
        createClose();
        createOption();
    }

    function mouseClickCloseButton() {
        removeClose();
        closeBlock();
        showItems();
        removeOption();
    }

    function mouseClickOptionColor(option) {
        return function (event) {
            var idNum = option.id.substring(6, 8), color;
            color = colors[idNum];
            setColor = color;
            changeColor(event.x, event.y, event.button);
        };
    }

    function mouseClickOptionPattern(pattern) {
        return function (event) {
            console.log(pattern);
            changePattern(event.x, event.y, event.button, pattern);
        };
    }

    function mouseClickOptionVolume(div) {
        return function (event) {

            console.log("div: " + div.id);

            if (div.id === "option0") {
                duckVolume = "kindvriendelijke shampoo";
            }
            if (div.id === "option1") {
                duckVolume = "Bad olie";
            }
            if (div.id === "option2") {
                duckVolume = "Shampoo";
            }
            if (div.id === "option3") {
                duckVolume = "Badzout";
            }
        };
    }

    function mouseClickOptionAccessories(div) {
        return function (event) {
            if (div.id === "option0") {
                duckAccessoires = "duckiewithhat.obj";
                console.log("duckiewithhat");
                viewer.setParameter('SceneUrl', '../duckie/duckiewithhat.obj');
                viewer.init();

            }
            if (div.id === "option1") {
                duckAccessoires = "duckiewithheart.obj";
                console.log("duckiewithheart");
                viewer.setParameter('SceneUrl', '../duckie/duckiewithheart.obj');
                viewer.init();
            }
            if (div.id === "option2") {
                console.log("evil");
                duckAccessoires = "duckieevil.obj";
                viewer.setParameter('SceneUrl', '../duckie/duckieevil.obj');
                viewer.init();
            }
            if (div.id === "option3") {
                console.log("duckie");
                duckAccessoires = "duckie.obj";
                viewer.setParameter('SceneUrl', '../duckie/duckie.obj');
                viewer.init();
            }
        }
    }

    function mouseClickOptionSize(div) {
        return function (event) {
            if (div.id === "option0") {
                console.log("10 cm");
                duckSize = 10;
            }
            if (div.id === "option1") {
                console.log("5 cm");
                duckSize = 5;
            }
            if (div.id === "option2") {
                console.log("20 cm");
                duckSize = 20;
            }
            if (div.id === "option3") {
                console.log("15 cm");
                duckSize = 15;
            }

        }
    }

    function hideItems(blocks) {
        for (i = 0; i < blocks.length; i = i + 1) {
            blocks[i].setAttribute('class', 'icon hidden');
            blocks[i].parentNode.setAttribute('class', 'item hidden');
        }
    }

    function showItems() {
        var icons = document.getElementsByClassName("icon hidden"),
            items = document.getElementsByClassName("item hidden");

        for (i = 0; i < items.length; i) {
            items[i].setAttribute('class', 'item');
            icons[i].setAttribute('class', 'icon');
        }
    }

    function createBlock() {
        var div, square;
        div = document.createElement("div");
        div.setAttribute('class', 'selectionBox');
        square = document.getElementById("squareRight");
        square.appendChild(div);
    }

    function closeBlock() {
        var div = document.getElementsByClassName("selectionBox");
        div[0].parentNode.removeChild(div[0]);
    }

    function createClose() {
        var div = document.createElement("div"),
            bar = document.createElement("div"),
            block = document.getElementsByClassName("selectionBox")[0],
            img = document.createElement("img");
        img.setAttribute('class', 'closeButton');
        img.setAttribute('id', 'closeButton');
        img.setAttribute('src', 'images/close.png');
        img.onmousedown = mouseClick(img, img);
        bar.setAttribute('class', 'bar');
        bar.setAttribute('id', 'barCloseButton');
        bar.appendChild(img);
        block.appendChild(bar);
    }

    function removeClose() {
        var div = document.getElementsByClassName("selectionBox"),
            bar = document.getElementById("barCloseButton");
        div[0].removeChild(bar);
    }

    function createOption() {
        var div, div2, square, colorPalette;
        square = document.getElementsByClassName("selectionBox")[0];
        div = document.createElement("div");
        div2 = document.createElement("div");
        colorPalette = document.createElement("div");
        colorPalette.setAttribute('class', 'colorPalette');
        colorPalette.setAttribute('id', 'colorPalette');
        if (setItem === "item2") {                  //colors

            for (i = 0; i < 15; i = i + 1) {
                div = document.createElement("div");
                div.setAttribute('class', 'option');
                div.setAttribute('id', 'option' + i);
                div.setAttribute('style', 'background-color:' + colors[i]);
                div.onmousedown = mouseClickOptionColor(div);
                colorPalette.appendChild(div);
            }

            div = document.createElement("div");
            div.setAttribute('class', 'moreColors');
            div.setAttribute('id', 'moreColors');
            div.onmousedown = preview();
            div.innerHTML = "Meer kleuren!";
            square.appendChild(colorPalette);
            square.appendChild(div);
        }

        if (setItem === "item1") {                  //pattern
            for (i = 0; i < 16; i = i + 1) {
                div = document.createElement("div");
                div.setAttribute('class', 'option');
                div.setAttribute('id', 'option' + i);
                div.setAttribute('style', 'background-image: url(\'patterns/' + patterns[i] + '\'); background-size: contain');
                div.onmousedown = mouseClickOptionPattern(patterns[i]);
                colorPalette.appendChild(div);

            }
            square.appendChild(colorPalette);
        }
        if (setItem === "item3") {                  //volume
            for (i = 0; i < 4; i = i + 1) {
                var img = document.createElement("img");
                div = document.createElement("div");
                div.setAttribute('class', 'optionSize');
                div.setAttribute('id', 'option' + i);
                img.setAttribute('src', 'images/' + filling[i]);
                div.onmousedown = mouseClickOptionVolume(div);
                div.appendChild(img);
                colorPalette.appendChild(div);

            }
            square.appendChild(colorPalette);
        }
        if (setItem === "item4") {                  //accessoires
            for (i = 0; i < 4; i = i + 1) {
                var img = document.createElement("img");
                div = document.createElement("div");
                div.setAttribute('class', 'optionSize');
                div.setAttribute('id', 'option' + i);
                img.setAttribute('src', 'images/' + accessoires[i]);
                div.onmousedown = mouseClickOptionAccessories(div);
                div.appendChild(img);
                colorPalette.appendChild(div);

            }
            square.appendChild(colorPalette);
        }

        if (setItem === "item5") {                  //accessoires
            var div = document.createElement("div");
            var input = document.createElement("input");

            div.appendChild(input);
            colorPalette.appendChild(div);

        }
        square.appendChild(colorPalette);


        if (setItem === "item8") {                  //grootte
            for (i = 0; i < 4; i = i + 1) {
                img = document.createElement("img");
                div = document.createElement("div");
                div.setAttribute('class', 'optionSize');
                div.setAttribute('id', 'option' + i);
                img.setAttribute('src', 'images/' + sizes[i]);
                div.onmousedown = mouseClickOptionSize(div);
                div.appendChild(img);
                colorPalette.appendChild(div);

            }
            square.appendChild(colorPalette);
        }


    }


    function removeOption() {
        var button, square;
        //options = document.getElementsByClassName("option");
        button = document.getElementById("moreColors");
        square = document.getElementsByClassName("colorPalette")[0];
        //for (i = 0; i < options.length - 1; i) {
        square.parentNode.removeChild(button);
        square.parentNode.removeChild(square);

        //}
    }

    function iconEventHandlers() {
        var icon;
        for (i = 0; i < 9; i = i + 1) {
            icon = document.getElementById("item" + i);
            icon.onmouseover = changeIconToGif(icon);
            icon.onmouseout = changeIconToJpg(icon);
        }

    }

    function changeIconToGif(icon) {
        return function () {
            icon.src = icon.src.substr(0, icon.src.length - 3) + "gif";

        };
    }

    function changeIconToJpg(icon) {
        return function () {
            icon.src = icon.src.substr(0, icon.src.length - 3) + "jpg";
        };
    }

    function preview() {
        return function () {
            var div, canvas, square, colorpicker;
            square = document.getElementsByClassName("selectionBox")[0];

            colorpicker = document.getElementsByClassName('colorpicker');
            if (colorpicker.length === 0) {
                div = document.createElement("div");
                div.setAttribute('class', 'colorpicker');
                div.setAttribute('style', 'display:none');

                canvas = document.createElement("canvas");
                canvas.setAttribute('id', 'picker');
                canvas.setAttribute('var', '3');
                canvas.setAttribute('style', 'width="300px" height="300px"');
                canvas.onmousedown = 'setColor()';
                div.appendChild(canvas);
                square.appendChild(div);
                div = document.createElement("div");
                div.setAttribute('class', 'selectedColor');
                div.setAttribute('id', 'selectedColor');
                div.onmousedown = preview();
                square.appendChild(div);
            }
            previewEvent();
            createBack();
        };
    }

    function previewEvent() {
        removeOption();
        setPreview();
    }

    function changeColorColorwheel(color) {
        setColor = color;
    }

    //TERUG NAAR VORIGE: Van Colorwheel naar Kleurenselectie.
    function createBack() {
        var img = document.createElement("img"),
            bar = document.getElementById("barCloseButton"),
            back = document.getElementsByClassName("backButton");
        if (back.length === 0) {
            img.setAttribute('class', 'backButton');
            img.setAttribute('id', 'backButton');
            img.setAttribute('src', 'images/back.png');
            img.onmousedown = removeBack();
            bar.appendChild(img);
        }
    }

    function removeBack() {
        return function () {
            var div = document.getElementById("backButton"),
                bar = document.getElementById("barCloseButton"),
                color = document.getElementById("selectedColor"),
                canvas = document.getElementsByClassName("colorpicker")[0];
            bar.removeChild(div);
            bar.parentNode.removeChild(color);
            bar.parentNode.removeChild(canvas);
            createOption();
            setColor = getColor();
            changeColor(0, 0, 0);
        };
    }

    function setPreview() {
        bCanPreview = true;
        canvas = document.getElementsByClassName('colorpicker')[0];
        canvas.setAttribute('style', 'display:block');
    }

    function setColor() {
        return function () {
            console.log("CHECK!!!");
            setColor = hexColor;
            $('#option15').css('backgroundColor', setColor);
        };
    }

    function getColor() {
        return setColor;
    }


    //GET USER DETAILS
    $http({
        method: "GET",
        url: "/account"
    }).success(function (data) {
            $scope.users = usersService.users.get({email: data.user.email});
        });

    /*Het badeendje wordt opgeslagen. Er wordt een melding getoond als er iets niet klopt */
    $scope.save = function () {
        console.log(duckAccessoires);

        //EXPORT DUCKIE TO IMAGE
        var canvas = document.getElementById("canvas");
        var duckImage = (canvas.toDataURL("image/png"));

        $http({
            method: "GET",
            url: "/account"
        }).success(function (data) {
                var isVerified = data.isVerified;

                if (isVerified === true) {

                    console.log("savee")
                    if (color && duckSize !== undefined || duckPattern && duckSize !== undefined) {
                        console.log("Save Duck");
                        console.log("huidige kleur: " + color);
                        console.log("accessoires: " + duckAccessoires);
                        console.log("grote: " + duckSize);
                        console.log("volume: " + duckVolume);
                        console.log("pattern:" + duckPattern);
                        console.log("image:" + duckImage);
                        console.log("tekst:");


                        $http({
                            method: "POST",
                            url: "/configurator/",
                            data: {"color": color, "configImage": duckImage, "accessoires": duckAccessoires, "size": duckSize, "volume": duckVolume, "pattern": duckPattern, "image": 'test.jpg', "text": "test tekst"}
                        }).success(function (res) {
                                var id = $scope.get = res.doc._id;
                                console.log("duck added to: " + $scope.users.doc.email + ", met het id: " + id);

                                $scope.configurator = configService.configurator.get({_id: id});
                                var popupSucces = document.getElementById("popupSucces");
                                console.log(popupSucces);
                                popupSucces.setAttribute('style', 'display: block');

                                $http({
                                    method: "PUT",
                                    url: "/users/" + $scope.users.doc.email + "/addDuck",
                                    data: {"id": $scope.users.doc._id, "configId": id}
                                })



                            });
                    }
                    else {
                        var popupError = document.getElementById("popupErrorSize");
                        popupError.setAttribute('style', 'display: block');
                        //window.setInterval(hidePopups(), 3000);

                    }


                } else {
                    var popupError = document.getElementById("popupErrorLogin");
                    popupError.setAttribute('style', 'display: block');

                }

            });
    }

    $scope.product = function () {
        if ($scope.configurator === undefined) {
            var popupError = document.getElementById("popupErrorOrder");
            popupError.setAttribute('style', 'display: block');
        } else {
            console.log("make order to: " + $scope.users.doc._id);
            window.location = "#/transaction/id/" + $scope.configurator.doc._id;
        }
    }

}


/**
 * Get all requests
 * @param $scope
 * @param requestsService
 * @constructor
 */

function HomeCtrl($scope, usersService, $http) {


}
/*Regelt het inloggen van de gebruiker en geeft het bijbehorende partial */
function UserLoginCtrl($scope, $http, usersService) {
    var isVerified, partial, thisUser, password;
    $scope.loginCtrl = function () {
        document.getElementById("loginCtrl").setAttribute("style", "display:block");
    }
    usersService.users.get({action: "checkLogin"}, function (req) {

        isVerified = req.isVerified;
    });

    $scope.getPartial = function () {
        //console.log('getPartial ', isVerified);
        if (isVerified === undefined) {
            $http({
                method: "GET",
                url: "/account"
            }).success(function (data) {
                    isVerified = data.isVerified;

                    if (isVerified === true) {
                        partial = "/partials/logout.html";
                    } else {
                        partial = "/partials/login.html";
                    }
                    $scope.src = partial;
                    return partial;
                });
        }

        if (isVerified === true) {
            partial = "/partials/logout.html";

        }
        else {
            partial = "/partials/login.html";
        }
        $scope.src = partial;
        return partial;
    };

    $scope.login = function (loginForm) {
        console.log('login', loginForm)

        $http({
            method: "POST",
            url: "/myLogin",
            data: {"username": loginForm.username, "password": loginForm.password}
        }).success(function (data) {
                isVerified = data.isVerified;
                thisUser = loginForm.username;
                password = loginForm.password;

                if (isVerified === true) {
                    // load partial
                    partial = "/partials/logout.html";
                    $scope.users = usersService.users.get({email: loginForm.username});
                    $scope.src = partial;
                    // redirect to user area
                    window.location = "#/home/";
                }
                if (thisUser === "admin@weduck.nl" && password === "adminWeDuck") {
                    partial = "/partials/logoutAdmin.html";

                    window.location = "#/adminPanel/";
                    console.log("Welcome admin");
                } else {
                    console.log('No login...');
                    console.log(data);
                    window.location = "#/";
                }

            });
    };

    /* Laadt de accountpagina die bij de ingelogde gebruiker hoort aan de hand van het e-mailadres */
    $scope.goToAccount = function () {
        $http({
            method: "GET",
            url: "/account"
        }).success(function (data) {
                window.location = "#/users/" + data.user.email;
                console.log("user: " + data.user.email);
            });
    }

    /* Logt de gebruiker uit en stuurt hem naar de homepage */
    $scope.logout = function () {

        $http({
            method: "GET",
            url: "/logout"
        }).success(function (data) {
                isVerified = data.isVerified;

                if (isVerified === true) {
                    $scope.src = partial;

                    window.location = "#/home/";
                } else {
                    console.log('log out');
                    partial = "/partials/login.html";
                    window.location = "#/home/";
                }
            });
    };

    $scope.openLogin = function () {
        console.log("open login");
        document.getElementById('loginDiv').setAttribute('style', 'display:block;');
        document.getElementById('buttonDiv').setAttribute('style', 'display:none;');
    };

    $scope.cancelLogin = function () {
        console.log("cancel login");
        document.getElementById('loginCtrl').setAttribute('style', 'display:none;');

    };
    $scope.openForm = function () {
        console.log("forget password");
        document.getElementById('forgetDiv').setAttribute('style', 'display:block;');
    }

    $scope.forgotPassword = function (forgotForm) {
        $http({
            method: "GET",
            url: "/users"
        }).success(function (data) {
                var stopLoop = false;
                for (var i = 0; i < data.doc.length; i = i + 1) {
                    if (stopLoop == false) {
                        if (forgotForm.username !== data.doc[i].email) {
                        } else {
                            stopLoop = true;
                            console.log("id vd klant: " + data.doc[i]._id + ",emailadress vd klant: " + data.doc[i].email);
                            var newGenPassword = Math.random().toString(36).slice(-8);
                            $http({
                                method: "PUT",
                                url: "/users/" + data.doc[i].email + "/password",
                                data: {"password": newGenPassword}
                            }).success(function () {
                                    if (data.isVerified === true) {
                                        console.log("nieuw wachtwoord: " + newGenPassword);
                                    }
                                });
                            $http({
                                method: "PUT",
                                url: "/users/" + data.doc[i].email + "/sendPassword",
                                data: {"username": data.doc[i].email, "newPassword": newGenPassword}
                            }).success(function () {
                                    if (data.isVerified === true) {
                                        console.log("nieuw wachtwoord: " + newGenPassword);
                                    }
                                });
                        }
                    }
                }
            });

    }
    $scope.setNav = function (id) {
        var icons, icon, newImg;
        icons = document.getElementsByClassName("iconNavigation");
        $http({
            method: "GET",
            url: "/account"
        }).success(function (data) {
                var isVerified = data.isVerified;

                if (isVerified === true) {
                    for (var i = 0; i < icons.length; i++) {
                        newImg = icons[i].src.slice(0, -5)
                        newImg = newImg + "1.png";
                        icons[i].src = newImg;

                    }
                } else {
                    for (var i = 1; i < icons.length; i++) {
                        newImg = icons[i].src.slice(0, -5)
                        newImg = newImg + "1.png";
                        icons[i].src = newImg;
                    }
                }
                icon = document.getElementById(id);
                newImg = icon.childNodes[0].src.slice(0, -5)
                newImg = newImg + "2.png";
                icon.childNodes[0].src = newImg;
            });

    }
    $scope.setHover = function (id) {
        var icons, icon, newImg;
        icons = document.getElementsByClassName("iconNavigation");

        $http({
            method: "GET",
            url: "/account"
        }).success(function (data) {
                var isVerified = data.isVerified;

                if (isVerified === true) {
                    for (var i = 0; i < icons.length; i++) {
                        newImg = icons[i].src.slice(0, -5)
                        newImg = newImg + "1.png";
                        icons[i].src = newImg;

                    }
                } else {
                    for (var i = 1; i < icons.length; i++) {
                        newImg = icons[i].src.slice(0, -5)
                        newImg = newImg + "1.png";
                        icons[i].src = newImg;
                    }
                }
                icon = document.getElementById(id);
                newImg = icon.childNodes[0].src.slice(0, -5)
                newImg = newImg + "2.png";
                icon.childNodes[0].src = newImg;
            });
    }
}


/**
 * CREATE user
 * @param $scope
 * @param $state
 * @param $route
 * @param db
 * @constructor
 *
 */


/**
 * GET all users
 * @param $scope
 * @param db
 * @constructor
 */
function UserListCtrl($scope, usersService) {
    console.log('Get all users.');
    $scope.users = usersService.users.get();
}


