"use strict";

angular.module('myApp.services', ['ngResource'])
    .factory('usersService', ['$resource', '$http',
        function ($resource) {
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'delete': {method: 'DELETE'}
                },
                db = {};
            db.users = $resource('/users/:email', {}, actions);
            return db;
        }
    ])

    .factory('configService', ['$resource', '$http',
        function ($resource) {

            var actions = {
                'get' : {method: 'GET'},
                'save' : {method: 'POST'},
                'update' : {method: 'PUT'},
                'query' : {method: 'GET', isArray: true},
                'delete' : {method: 'DELETE'}
            }, db = {};
            db.configurator = $resource('/configurator/:_id', {}, actions);
            return db;
        }
    ])
    .factory('transactionService', ['$resource', '$http',
        function ($resource) {

            var actions = {
                'get' : {method: 'GET'},
                'save' : {method: 'POST'},
                'update' : {method: 'PUT'},
                'query' : {method: 'GET', isArray: true},
                'delete' : {method: 'DELETE'}
            }, db = {};
            db.transactions = $resource('/transactions/:_id', {}, actions);
            return db;
        }
    ])

    .factory('productService', ['$resource', '$http',
        function ($resource) {

            var actions = {
                'get' : {method: 'GET'},
                'save' : {method: 'POST'},
                'update' : {method: 'PUT'},
                'query' : {method: 'GET', isArray: true},
                'delete' : {method: 'DELETE'}
            }, db = {};
            db.product = $resource('/product/:_id', {}, actions);
            return db;
        }
    ])

    .factory('wallService', ['$resource', '$http',
        function ($resource) {

            var actions = {
                'get' : {method: 'GET'},
                'save' : {method: 'POST'},
                'update' : {method: 'PUT'},
                'query' : {method: 'GET', isArray: true},
                'delete' : {method: 'DELETE'}
            }, db = {};
            db.wallofduck = $resource('/wall/:_id', {}, actions);
            return db;
        }
    ])

    .factory('configEditService', ['$resource', '$http',
        function ($resource) {

            var actions = {
                'get' : {method: 'GET'},
                'save' : {method: 'POST'},
                'update' : {method: 'PUT'},
                'query' : {method: 'GET', isArray: true},
                'delete' : {method: 'DELETE'}
            }, db = {};
            db.configuratorEditor = $resource('/configEdit/:_id', {}, actions);
            return db;
        }
    ])

    .factory('contactService', ['$resource', '$http',
        function ($resource) {

            var actions = {
                'get' : {method: 'GET'},
                'save' : {method: 'POST'},
                'update' : {method: 'PUT'},
                'query' : {method: 'GET', isArray: true},
                'delete' : {method: 'DELETE'}
            }, db = {};
            db.contact = $resource('/contact/:email', {}, actions);
            return db;
        }
    ]);



