/**
 * Created by theotheu on 30-10-13.
 */
/**
 * @see web http://docs.angularjs.org/guide/directive
 * @see video http://egghead.io/lessons/angularjs-first-directive
 */
angular.module('myApp.directives', [])
    .directive('confirmationNeeded', function () {
        return {
            priority: 1,
            terminal: true,
            link: function (scope, element, attr) {
                var msg = attr.confirmationNeeded || "Are you sure?";
                var clickAction = attr.ngClick;
                element.bind('click',function () {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    });