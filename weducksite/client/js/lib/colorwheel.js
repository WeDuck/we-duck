/**
 *
 * HTML5 Color Picker
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2012, Script Tutorials
 * http://www.script-tutorials.com/
 */

$(function () {
    "use strict";
    var bCanPreview = true, canvas, setColor, hexColor,colorwheel = {

        init: function () {
            var ctx, image, imageSrc, canvasOffset, canvasX, canvasY, imageData, pixel, pixelColor, dColor;
            // create canvas and context objects
            canvas = document.getElementById('picker');
            ctx = canvas.getContext('2d');

            // drawing active image
            image = new Image();
            image.onload = function () {
                ctx.drawImage(image, 0, 0, image.width, image.height); // draw the image on the canvas
                console.log("Img!");
            };

            // select desired colorwheel
            imageSrc = 'images/colorwheel1.png';
            switch ($(canvas).attr('var')) {
            case '2':
                imageSrc = 'images/colorwheel2.png';
                break;
            case '3':
                imageSrc = 'images/colorwheel3.png';
                break;
            case '4':
                imageSrc = 'images/colorwheel4.png';
                break;
            case '5':
                imageSrc = 'images/colorwheel5.png';
                break;
            }
            image.src = imageSrc;
            $('#picker').mousemove(function (e) { // mouse move handler
                if (bCanPreview) {
                    // get coordinates of current position
                    canvasOffset = $(canvas).offset();
                    canvasX = Math.floor(e.pageX - canvasOffset.left);
                    canvasY = Math.floor(e.pageY - canvasOffset.top);

                    // get current pixel
                    imageData = ctx.getImageData(canvasX, canvasY, 1, 1);
                    pixel = imageData.data;

                    // update preview color
                    pixelColor = "rgb(" + pixel[0] + ", " + pixel[1] + ", " + pixel[2] + ")";
                    //$('.preview').css('backgroundColor', pixelColor);
                    //$('#option15').css('backgroundColor', setColor);



                    // update controls
                    $('#rVal').val(pixel[0]);
                    $('#gVal').val(pixel[1]);
                    $('#bVal').val(pixel[2]);
                    $('#rgbVal').val(pixel[0] + ',' + pixel[1] + ',' + pixel[2]);

                    dColor = pixel[2] + 256 * pixel[1] + 65536 * pixel[0];
                    $('#hexVal').val('#' + ('0000' + dColor.toString(16)).substr(-6));
                    hexColor = '#' + ('0000' + dColor.toString(16)).substr(-6);
                    changeColorColorwheel(hexColor);
                    changeColor(0, 0, 0);
                }
            });
        },

        setPreview: function () {
            bCanPreview = true;
            canvas = document.getElementsByClassName('colorpicker')[0];
            canvas.setAttribute('style', 'display:block');
        },

        setColor: function () {
            return function () {
                console.log("CHECK!!!");
                setColor = hexColor;
                $('#option15').css('backgroundColor', setColor);
            };
        },

        getColor: function () {
            return setColor;
        }
    };
    window.colorwheel = colorwheel;
    //colorwheel.init();
}());