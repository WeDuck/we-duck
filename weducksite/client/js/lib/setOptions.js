(function () {
    "use strict";
    var setOptions = {

        init: function () {

        },

        color: function () {
            var colors;
            colors = ["#69CCE9", "#E89A65", "#FEE57E", "#AB8AB5", "#76F8E4", "#E17E79", "#F5C18D", "#F89CF9", "#FFFF00", "#9bff3c", "#6E6E6E", "#4B088A", "#F3E2A9", "#FE9A2E", "#81F7BE", "#FFFF00"];
            return colors;
        },

        pattern: function () {
            var patterns;
            patterns = ["patterns01.png", "patterns02.png", "patterns03.png", "patterns04.png", "patterns05.png", "patterns06.png", "patterns07.png", "patterns08.png", "patterns09.png", "patterns10.png", "patterns11.png", "patterns12.png", "patterns13.png", "patterns14.png", "patterns15.png"];
            return patterns;
        },

        image: function () {
            var images;
            images = ["duck1.png", "duck2.png", "duck3.png", "duck4.png", "duck5.png", "duck6.png", "duck7.png", "duck8.png", "duck9.png", "duck10.png", "duck11.png", "duck12.png", "duck13.png", "duck14.png", "duck15.png", "duck16.png", "duck17.png", "duck18.png"];
            return images;
        },

        link: function () {
            var link;
            link = ["52d68729afb258a14d000006", "52d686aeafb258a14d000005", "52d6879aafb258a14d000007", "52d68be6afb258a14d000013", "52d68811afb258a14d000008", "52d6883eafb258a14d000009", "52d688d2afb258a14d00000a", "52d6890dafb258a14d00000b", "52d68944afb258a14d00000c", "52d6897fafb258a14d00000d", "52d6899bafb258a14d00000e", "52d689a7afb258a14d00000f", "52d68a41afb258a14d000010", "52d68a57afb258a14d000011", "52d68a76afb258a14d000012", "52d68cd9afb258a14d000014", "52d68d57afb258a14d000015", "52d68f13afb258a14d000016"];
            return link;
        },

        text: function () {

        },

        accessoire: function () {
            var accessoires;
            accessoires = ["hat.png", "heart.png", "devil.png", "geenaccessoires.png"];
            return accessoires;
        },

        filling: function () {
            var filling;
            filling = ["kindershampoo.png", "badolie.png", "shampoo.png", "zout.png"];
            return filling;

        },

        size: function () {
            var sizes;
            sizes = ["size10.png", "size5.png", "size20.png", "size15.png"];
            return sizes;
        },

        save: function () {

        },

        order: function () {

        }
    };

    window.setOptions = setOptions;
    setOptions.init();


}());
